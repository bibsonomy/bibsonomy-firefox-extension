Wie erstellte ich eine Erweiterung (.xpi) aus diesen Dateien?
	Packe die Dateien: chrome.manifest/install.rdf und Ordner chrome/defaults/searchplugin in ein
	Zip-Archiv und ändere die Endung von .zip in .xpi um.
	
Wie installiere ich die .xpi Datei?
	str+o und die Datei auswählen.
	
Ordnerstruktur:
	"->" = Ordner
	"-" = Datei
 
-> chrome 
	-> content : Hauptverzeichnis, beinhaltet .js/.xul-Dateien um alles zu erstellen/steuern/modifizieren
		Bedeutung der Dateien mit folgenden Endungen:
			.xul : 	erzeugt fenster/ neue einträge in bestehende fenster. 
					Importiert die jeweiligen JavaScript-Dateien die die Eingaben verarbeiten.
			.js : 	steuert/modifiziert die Erweiterung 
			*Overlay.js : Erweitert die Firefox-Oberfläche (Um: Nav-, Tool-, Sidebar, Context-Menu, Einstellungen etc. ).
			*Control.js : Der 'Controller' der die Oberfläche und deren Eingaben verarbeitet/steuert/modifiziert.

		-> navbar/sidebar/toolb : Der jeweilige Ordner beinhaltet alles, was für die Navigation-, Side- und Toolbar notwendig ist.
									(jeweils eine Overlay.js-Datei und ein Control.js-Datei)			
		-> sidebar beinhaltet zus�tzlich:
			- sidebar.xul : Grundger�st f�r die Sidebar. Beinhaltet Suche/Tag-Liste/Lesezeichen-Liste, wobei die Tag- und Lesezeichenlisten
							mit der SidebarTree.js Klasse aufgebaut werden m�ssen.
			- SidebarTree.js : Klasse um die Sidebarlisten (Tagcloud/Taglist/Lesezeichen) aufzubauen/zu modifizieren
		
		- preferences.xul : 'Einstellungen'
		- preferencesControl.js : Verarbeitet Eingaben die in den Einstellungen vorgenommen werden.

		- Preferences.js : Klasse um auf die Firefox-API für 'preferences' zuzugreifen. Z.B. für Benutzername/Api-Key/Server
		
		- statusbar.xul : Zum Import/Synchronisierung vom Server. Der Import/die Synchronisierung geschieht in der BookmarkDB.js
		- firefoxImport.xul : Um Lesezeichen aus dem Firefox auf Bibsonomy zu speichern
		- firefoxImportControl.js : Führt den Firefox-Import aus.

		- postBookmark.xul : Zum Bearbeiten/Erstellen von Lesezeichen.
		- postBookmarkControl.js : Verarbeitet die Eingabe
		
		- observer.js : Wird nicht benutzt. Kann aber später verwendet werden um den Menu-Ornder zu überwachen.

		- BookmarkDB.js : Die wohl wichtigste Datei. Die Aufgaben sind in erster Linie 
							der Import(vom Server)/Synchronisierung	und das Erstellen/Bearbeiten 
							der Lesezeichen. Weiterhin werden die Datenbank- (Places/Eigene) 
							und Serverkommunikation �ber diese Klasse ausgef�hrt. 
							
	->locale : Kann verwendet werden um verschiedene Sprachen zu unterstüzen (nicht verwendet)
	->skin : Enthält Grafiken und CSS-Dateien.
		->navbar: für die Navigaionbar
		->sidebar: für die Sidebar 
		->toolbar: für die Toolbaar
		->classic->bibsonomy: weitere grafiken/css-dateien (nav-, side-, toolbar brauchen diese nicht) 

-> defaults -> preferences: Beinhaltet die Starteinstellungen (Preferences)

-> searchplugins: Erstellt den Schnellsuche-Eintrag 