pref("extensions.bibsonomy.homeButton", "true");
pref("extensions.bibsonomy.sidebarButton", "true");
pref("extensions.bibsonomy.username", "");
pref("extensions.bibsonomy.apikey", "");
pref("extensions.bibsonomy.server","http://www.bibsonomy.org/");
pref("extensions.bibsonomy.defaultfolder","Bibsonomy Bookmarks");
pref("extensions.bibsonomy.defaultfolderid","");
pref("extensions.bibsonomy.firstImport","true");
pref("extensions.bibsonomy.showurl", "true");
pref("extensions.bibsonomy.tagview", "0");
pref("extensions.bibsonomy.newtab", "true");
pref("extensions.bibsonomy.starbutton", "false");
pref("extensions.bibsonomy.relation", "0");
pref("extensions.bibsonomy.scraperDate", "0");
pref("extensions.bibsonomy.scraper", "");
pref("extensions.bibsonomy.bookmarkDisplayLimit", 100);
