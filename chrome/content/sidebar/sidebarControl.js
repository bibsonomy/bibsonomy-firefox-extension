var	bookmarkScrollListLimit=5;
var bookmarklimit = Preferences.getSidebarBookmarkLimit();
var bookmarkstart = 0;
var bookmarkpage = '';
var bookmarkId = -1;
var bookmarkdb = new BookmarkDB();
var sidebar = new SidebarTree();
var tagcloud = document.getElementById('tagbox'); 
var taglist = document.getElementById('treeTagChildren');
var relationslist = document.getElementById('relations');
var bookmarklist = document.getElementById('bookmarkList');
document.getElementById('cbToggleurl').checked = !Preferences.getShowurl();
var timer;

var lastBookmarkFunction = ""; // tag/all/search/update

var visibleBookmarks;
var searchText = '';
var selectedTag = '';

// search box functions
function timeout(){
	clearTimeout(timer); 
	timer = setTimeout("search()",1000);
}

function updateSearchText(){
	searchText = document.getElementById("searchBox").value;
	searchText = searchText.basicTrim();
}

// bookmarkscrollinglist
function nextBookmarks(){
	bookmarkstart+=bookmarklimit;
	if(visibleBookmarks.length < bookmarkstart){
		bookmarkstart-=bookmarklimit;
	}
	buildBookmarkList();
}

function previousBookmarks(){
	bookmarkstart-=bookmarklimit;
	if(bookmarkstart<0){
		bookmarkstart=0;
	}
	buildBookmarkList();
}

//-----BOOKMARKLIST-BUILDER-------//
function search(){
	selectedTag = '';
	bookmarkstart = 0;
	
	updateSearchText();
	if (searchText.length > 1){
		visibleBookmarks = bookmarkdb.search(searchText,
				document.getElementById("cbUrl").checked,
				document.getElementById("cbTitle").checked,
				document.getElementById("cbDescription").checked,
				document.getElementById("cbTags").checked);
		showTags(searchText);
	} else {
		showTags();
	}
	buildBookmarkList();
}

function showAllBookmarks(){
	selectedTag = '';
	searchText = '';
	bookmarkstart = 0;
	
	visibleBookmarks = bookmarkdb.getAllBookmarks();
	buildBookmarkList();
}

function showBookmarksForTag(){
	searchText = '';
	bookmarkstart = 0;
	
	visibleBookmarks = bookmarkdb.getBookmarksForTag(selectedTag);
	buildBookmarkList();
}

function updateBookmarks(){
	var bookmarks = new Array();
	for(var i = 0; i < visibleBookmarks.length; i++){
		bookmarks.push(visibleBookmarks[i][0]);
	}
	visibleBookmarks = bookmarkdb.getAllBookmarks('','',bookmarks); // bookmarks should be limited already
	buildBookmarkList();
}


function buildBookmarkList(){

	// 'bookmark for:....' label
	if(searchText != ''){
		var string = searchText.length > 30 ? searchText.substr(0,25)+"..." :searchText;
		document.getElementById("bookmarksforLabel").setAttribute('value',"for: "+string);
	} else if(selectedTag != ''){
		var string = selectedTag.length > 30 ? selectedTag.substr(0,25)+"..." :selectedTag;
		document.getElementById("bookmarksforLabel").setAttribute('value',"for: "+string);
	} else {
		document.getElementById("bookmarksforLabel").setAttribute('value','');
	}
	
	// build the list to switch through the bookmarks
	buildBookmarkScrollList();
	
	// clear the bookmarklist and build up the list with the new selected bookmarks
	sidebar.clear(bookmarklist);
	if(visibleBookmarks.length === 0){
		sidebar.newBookmarklistElement(-1,"empty","empty","empty","empty",false);
	}
	for (var i=bookmarkstart; i < visibleBookmarks.length && i < bookmarkstart+bookmarklimit; i++) {
		sidebar.newBookmarklistElement(visibleBookmarks[i][0], visibleBookmarks[i][1],visibleBookmarks[i][3],visibleBookmarks[i][2], visibleBookmarks[i][4], Preferences.getShowurl());
	}
}

function buildBookmarkScrollList(){
	document.getElementById("prevList").setAttribute('disabled', 'false');
	document.getElementById("prevList").setAttribute('class', 'bookmarklisting');
	document.getElementById("nextList").setAttribute('disabled', 'false');
	document.getElementById("nextList").setAttribute('class', 'bookmarklisting');

	document.getElementById("pageList1").setAttribute('hidden', 'true');
	document.getElementById("pageList2").setAttribute('hidden', 'true');
	document.getElementById("pageList3").setAttribute('hidden', 'true');
	document.getElementById("pageList4").setAttribute('hidden', 'true');
	document.getElementById("pageList5").setAttribute('hidden', 'true');

	if(bookmarkpage != ''){
		var currentpage = bookmarkpage;
		bookmarkstart=(bookmarkpage-1)*bookmarklimit
		bookmarkpage='';
	} else {
		var currentpage = bookmarkstart/bookmarklimit+1;
	}
	var lastpage=Math.ceil(visibleBookmarks.length/bookmarklimit);
	var firstpagenumber = currentpage + (Math.floor(bookmarkScrollListLimit/2)*(-1));
	var lastpagenumber = currentpage + (Math.floor(bookmarkScrollListLimit/2));
	if(firstpagenumber<1){//first pages
		firstpagenumber=1;
		lastpagenumber=bookmarkScrollListLimit;
	}
	if(lastpagenumber>lastpage){//last pages
		firstpagenumber=lastpage-bookmarkScrollListLimit+1;
		lastpagenumber=lastpage;
	}
	if(visibleBookmarks.length < ((bookmarkScrollListLimit-1)*bookmarklimit)){//less than bookmarkscrolllistlimit rows
		firstpagenumber=1;
		lastpagenumber=Math.ceil(visibleBookmarks.length/bookmarklimit);
	}
	for(var j=1, i=firstpagenumber;i<=lastpagenumber;i++,j++){
		if(i==currentpage){
			var sClass = "bookmarklistingselected";
		} else {
			var sClass = "bookmarklisting";
		}
		document.getElementById("pageList"+(j)).setAttribute('class', sClass);
		document.getElementById("pageList"+(j)).setAttribute('hidden', 'false');
		document.getElementById("pageList"+(j)).setAttribute('value', i);
		document.getElementById("pageList"+(j)).setAttribute('onmouseup', "bookmarkpage="+i+";buildBookmarkList();");
	}
}

//------------//

// hide/unhide url
function toggleUrl(){
	Preferences.setShowurl(!Preferences.getShowurl());
	updateBookmarks();
}

function updateTags(searchText){
	showTags(searchText);
}

// build/rebuild the taglist or tagcloud
function showTags(str){
	document.getElementById('tagTab').selectedIndex = Preferences.getTagview();
	if(Preferences.getTagview() == 0 && document.getElementById('tagcloudTabpanel').getAttribute('value') == 'true'){ // Tagcloud
		document.getElementById('tagcloudTabpanel').setAttribute('value','false');
		sidebar.clear(tagcloud);
		var tagsToShow = 50; // count of tags to display in the tagcloud
		var tagCoud;
		if(str == undefined || str == '' || document.getElementById("cbTags").checked == false){
			tagCloud = bookmarkdb.getCloudTags(tagsToShow);
		} else {
			tagCloud = bookmarkdb.getCloudTagsLike(tagsToShow, str);
		}
		
		if(tagCloud.length < 1)
			return;
			
		if(tagCloud.length < tagsToShow)
			tagsToShow = tagCloud.length;
			 
		var lowest = tagCloud[0][1];
		var highest = 0;
		for(var i = 0; i < tagCloud.length ;i++){
			if(parseInt(tagCloud[i][1]) > highest){
				highest = tagCloud[i][1];
			}
			if(parseInt(tagCloud[i][1]) < lowest){
				lowest = tagCloud[i][1];
			}
		}

		for (var j=0; j < tagCloud.length; j++) {
			var size = (parseFloat(tagCloud[j][1]) - lowest) / (highest - lowest);
			var style = null;
			if(size > 0.666){
				style = "big";
			} else if(size > 0.2){
				style = "normal";
			} else {
				style = "small";
			}
			sidebar.newTagcloudElement(tagCloud[j][0], style);
		}
	} else if(Preferences.getTagview() == 1 && document.getElementById('taglistTabpanel').getAttribute('value') == 'true'){ // Taglist
		document.getElementById('taglistTabpanel').setAttribute('value','false');
		sidebar.clear(taglist);
		var tagList;
		if(str == undefined || str == ''|| document.getElementById("cbTags").checked == false){
			tagList = bookmarkdb.getTaglist();
		} else {
			tagList = bookmarkdb.getTaglistLike(str);
		}

		for (var i=0; i < tagList.length; i++) {
			sidebar.newTaglistElement(tagList[i][0], tagList[i][1]);
		}
	} else if(Preferences.getTagview() == 2){ // relations
		sidebar.clear(relationslist);
		var relations = bookmarkdb.getRelations();
		for(var i = 0; i < relations.length; i++){
			sidebar.newRelationElement(relations[i].split(" "));
		}
	}
}

// toggle between tagcloud and taglist
function toggleTagview(){
	Preferences.setTagview(document.getElementById('tagTab').selectedIndex);
	showTags(searchText);
}

/* 
 * menu functions
 */
 
// update relations
function importRelations(){
	bookmarkdb.getRelations();
	showTags();
}
 
// load new page from selected bookmark
function openUrl(){
	if(bookmarkId == -1){
		return -1;
	}
	
	// open in new tab
	var mainWindow = window.QueryInterface(Components.interfaces.nsIInterfaceRequestor)
	.getInterface(Components.interfaces.nsIWebNavigation)
	.QueryInterface(Components.interfaces.nsIDocShellTreeItem)
	.rootTreeItem
	.QueryInterface(Components.interfaces.nsIInterfaceRequestor)
	.getInterface(Components.interfaces.nsIDOMWindow);
	
	if(Preferences.isNewtab()){
		var wm = Components.classes["@mozilla.org/appshell/window-mediator;1"]
		                              .getService(Components.interfaces.nsIWindowMediator);
		var browserEnumerator = wm.getEnumerator("navigator:browser");
		var tabbrowser = browserEnumerator.getNext().getBrowser(); 
		var newTab = tabbrowser.addTab(bookmarkdb.getUrl(bookmarkId));
		tabbrowser.selectedTab = newTab;
		tabbrowser.focus();
	} else {
		mainWindow.getBrowser().selectedTab.linkedBrowser.contentDocument.location = bookmarkdb.getUrl(bookmarkId);
	}
}

// edit the selected bookmark
function editBookmark(){
	if(bookmarkId == -1){
		return -1;
	}

	// open the 'post bookmark' window
	var features = "chrome,titlebar,toolbar,centerscreen,modal";
	window.openDialog("chrome://bibsonomy/content/postBookmark.xul", "post bookmark",
		features,
		bookmarkdb.getUrl(bookmarkId),
		"",
		null);
	updateSearchText();
	updateBookmarks();
	showTags(searchText);
}


// delete the selected bookmark 
function deleteTreeBookmark(){
	if(bookmarkId == -1){
		return -1;
	}
	// delete bookmark from own db
	bookmarkdb.deleteBookmark(bookmarkId);
	updateSearchText();
	updateBookmarks();
	showTags(searchText);
}


/*
 * listener functions
 */

// handles the selection of a tag  
function updateUpTag(event){
	if(document.getElementById('treeTag').boxObject.width - event.pageX < 14){
		return;
	}
	if(event.button == 0){
		var row = {}, column = {}, part = {};
		var tree = document.getElementById("treeTag");
		var boxobject = tree.boxObject;
		boxobject.QueryInterface(Components.interfaces.nsITreeBoxObject);
		boxobject.getCellAt(event.clientX, event.clientY, row, column, part);

		if(row.value == -1){
			return -1;
		}
		var treeTagcells = document.getElementById('treeTagChildren').getElementsByTagName('treecell');
		tag = treeTagcells[row.value*2].getAttribute('id');
		setSelectedTag(tag);
	}
}

function setSelectedTagEscaped(tag){
	setSelectedTag(unescape(tag));
}

function setSelectedTag(tag){
	selectedTag = tag;
	showBookmarksForTag();
}

// left click on the bookmark
function updateUp(event){
	if(document.getElementById('bookmarkList').boxObject.width - event.pageX < 14){
		return;
	}
	setBookmarkIdFromEvent(event);
	if(event.button == 0){
		if(bookmarkId == -1){
			return -1;
		}
		openUrl(event);
	}
}

function advancedBookmarkOverview(){
	var features = "menubar=no,location=yes,resizable=yes,scrollbars=yes,status=yes";
	var abo = window.openDialog(	"chrome://bibsonomy/content/advancedBookmarkOverview.xul", 
			"Advanced Bookmark Overview",
			features);
	abo.focus();
}

// return the id from an event (only for richlistitem)
function setBookmarkIdFromEvent(event){
	bookmarkId = event.currentTarget.getAttribute('id');
}

String.prototype.basicTrim = function() {
	return(this.replace(/\s+$/,"").replace(/^\s+/,""));
};

var tagTab = document.getElementById("tagTab");
tagTab.addEventListener('command',toggleTagview,true);

var cbTags = document.getElementById("cbTags");
cbTags.addEventListener('command',search,true);

var cbDescription = document.getElementById("cbDescription");
cbDescription.addEventListener('command',search,true);

var cbUrl = document.getElementById("cbUrl");
cbUrl.addEventListener('command',search,true);

var cbTitle = document.getElementById("cbTitle");
cbTitle.addEventListener('command',search,true);

//var button = document.getElementById("advancedBookmarkOverview");
//button.addEventListener('command',advancedBookmarkOverview,true);