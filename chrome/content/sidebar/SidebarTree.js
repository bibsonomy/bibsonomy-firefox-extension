function SidebarTree(target) {

	this.newBookmarklistElement = function(id, name, url, description, tags, showurl){
		if(id== -1){
			var richlistitem = document.getElementById("bookmarkList").appendChild(document.createElement('richlistitem'));
			var grid = richlistitem.appendChild(document.createElement("grid"));
			var rows = grid.appendChild(document.createElement("rows"));
			var row1 = rows.appendChild(document.createElement("row"));
			var label1 = row1.appendChild(document.createElement("label"));
			label1.setAttribute('value', name);
    	    label1.setAttribute('style','padding-left:20px');
			return;
		}
        var re = new RegExp('^(?:f|ht)tp(?:s)?\://([^/]+)', 'im');
        var topLevelDomain = "http://" + url.match(re)[1].toString();
		var faviconservice = Components.classes["@mozilla.org/browser/favicon-service;1"] 
										.getService(Components.interfaces.nsIFaviconService); 
		var pageURI = Components.classes["@mozilla.org/network/io-service;1"]
			.getService(Components.interfaces.nsIIOService)
			.newURI(topLevelDomain, null, null);

		var nsIFavicon = faviconservice.getFaviconImageForPage(pageURI);
		
		var richlistitem = document.getElementById("bookmarkList").appendChild(document.createElement('richlistitem'));
		richlistitem.setAttribute('id', id);
		richlistitem.setAttribute('style',	'border-bottom:1px dotted; background-image:url("' + nsIFavicon.spec + '"); ' + 
											'background-repeat:no-repeat;  ' +
											'background-position: 1px 1px; ' +
											'border-bottom:1px dotted; ' +
											'overflow: hidden; ');
		richlistitem.setAttribute('class', 'richlistitem');
		richlistitem.setAttribute('context', 'treeBookmarkClipmenu');
		if(description != ''){
			richlistitem.setAttribute('tooltiptext', description + "\n" + tags);
		} else {
			richlistitem.setAttribute('tooltiptext', tags);
		}
		richlistitem.setAttribute('onclick','updateUp(event);');

		var grid = richlistitem.appendChild(document.createElement("grid"));
		var rows = grid.appendChild(document.createElement("rows"));

		var row1 = rows.appendChild(document.createElement("row"));
		var label1 = row1.appendChild(document.createElement("label"));
		label1.setAttribute('value', name);
        label1.setAttribute('style','padding-left:20px');
		if(showurl){
			var row2 = rows.appendChild(document.createElement("row"));
			var label2 = row2.appendChild(document.createElement("label"));
			label2.setAttribute('value', url);		
	        label2.setAttribute('class', 'url');
			label2.setAttribute('properties', 'link');
			label2.setAttribute('style','padding-left:20px');
		}
	}
	
	this.newTagcloudElement = function(name,style){
		var tagbox = document.getElementById("tagbox");
		var li = tagbox.appendChild(document.createElement("html:li"));
		li.setAttribute('class','tagcloudLI');
		var a = li.appendChild(document.createElement("html:a"));
		a.setAttribute('id', name);
		a.setAttribute('class',style );
		a.setAttribute('style', "cursor: pointer;")
		a.setAttribute('onmouseup', "setSelectedTagEscaped('" + escape(name) + "');");
		a.appendChild(document.createTextNode(name));

		/* muster
		<html:ul class="tagcloud" id="tagbox">
			<html:li class="tagten">
			 		<html:a title="31 posts" onmouseover="" style="font-size: 230%;"
					onmouseup="alert('hello');" href="/tag/bookmarking">bookmarking</html:a>
			</html:li>
		</html:ul>'*/
	};

	this.newTaglistElement = function(name, count){
		var treeTagItem = document.getElementById('treeTagChildren').appendChild(document.createElement('treeitem'));
		treeTagItem.setAttribute('container', 'false');
		treeTagItem.setAttribute('open', false);
		treeTagItem.setAttribute('style','background-color: black');
		var treeTagRow = treeTagItem.appendChild(document.createElement('treerow'));

		var treeTagcell = treeTagRow.appendChild(document.createElement('treecell'));
		treeTagcell.setAttribute('id', name);
		treeTagcell.setAttribute('label', name);
		treeTagcell.setAttribute('properties', 'tag');
		
		var treeTagcell = treeTagRow.appendChild(document.createElement('treecell'));
		treeTagcell.setAttribute('id', name);
		treeTagcell.setAttribute('label', count + 'x');
		//treeTagcell.setAttribute('properties', 'tag');
		
	};
	
	this.newRelationElement = function(relations){
		var relationRichlistitem = document.getElementById('relations').appendChild(document.createElement('richlistitem'));
		relationRichlistitem.setAttribute('class', 'richlistitem');
		var relationDescription = relationRichlistitem.appendChild(document.createElement('description'));
		relationDescription.setAttribute('style','margin: 0px 0px 0px 10px;');
		relationDescription.setAttribute('value', relations[0]);
		relationDescription.setAttribute('class','relations');
		relationDescription.setAttribute('onmouseup','setSelectedTag("'+ relations.join(" ") + '")');
		var relationDescription = relationRichlistitem.appendChild(document.createElement('description'));
		relationDescription.setAttribute('style', 'background-image: url(chrome://bibsonomy/skin/sidebar/icons/arrow.png);'+
												  'padding: 5px 10px 5px 5px; background-repeat:no-repeat;');// arrow.png
		// list of tags related to it 
		for(var i = 1; i < relations.length; i++){
			var relationDescription = relationRichlistitem.appendChild(document.createElement('description'));
			relationDescription.setAttribute('value', relations[i]);
			relationDescription.setAttribute('class','relations');
			relationDescription.setAttribute('onmouseup','setSelectedTag("'+relations[i]+'")');
		}
		
		/* muster
		<richlistitem class='richlistitem'>
			<description value="tag1" class='relations' properties = 'link' onmouseup="alert('tag1')"/>
			<description value="--" />
			<description value="tag2" class='relations' properties = 'link' onmouseup="alert('tag2')" />
			<description value="tag3" class='relations' properties = 'link' onmouseup="alert('tag3')"/>
		</richlistitem> */
	}
	
	this.clear = function(element){
		while (element.firstChild) {
			element.removeChild(element.firstChild);
		}
	}
}