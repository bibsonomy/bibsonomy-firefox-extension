var blbub = {	"patterns" : 	[
		{
			"host" : ".*citebase.org",
			"path" : "null"
		},
		{
			"host" : "null",
			"path" : ".*?PRS=PP/PPN"
		},
		{
			"host" : ".*ieeexplore.ieee.org",
			"path" : "/xpl.*"
		},
		{
			"host" : ".*ieeexplore.ieee.org",
			"path" : "/books.*"
		},
		{
			"host" : ".*ieeexplore.ieee.org",
			"path" : "/xpl.*"
		},
		{
			"host" : ".*springerlink.com",
			"path" : "null"
		},
		{
			"host" : ".*sciencedirect.com",
			"path" : "/science.*"
		},
		{
			"host" : ".*ncbi.nlm.nih.gov",
			"path" : "null"
		},
		{
			"host" : ".*eutils.ncbi.nlm.nih.gov",
			"path" : "null"
		},
		{
			"host" : ".*pubmedcentral.nih.gov",
			"path" : "null"
		},
		{
			"host" : ".*slac.stanford.edu",
			"path" : "null"
		},
		{
			"host" : ".*www-library.desy.de",
			"path" : "null"
		},
		{
			"host" : ".*l3s.de",
			"path" : "null"
		},
		{
			"host" : ".*portal.acm.org",
			"path" : "null"
		},
		{
			"host" : ".*aifb.uni-karlsruhe.de",
			"path" : "/Personen/viewPerson.*"
		},
		{
			"host" : ".*aifb.uni-karlsruhe.de",
			"path" : "/Publikationen/exportPublikationenFG.bib.*"
		},
		{
			"host" : ".*aifb.uni-karlsruhe.de",
			"path" : "/Publikationen/exportPublikation.bib.*"
		},
		{
			"host" : ".*aifb.uni-karlsruhe.de",
			"path" : "/Publikationen/showPublikation.*"
		},
		{
			"host" : ".*aifb.uni-karlsruhe.de",
			"path" : "/Publikationen/showPublikation_english.*"
		},
		{
			"host" : ".*aifb.uni-karlsruhe.de",
			"path" : "/Publikationen/exportPublikationenPerson.bib.*"
		},
		{
			"host" : ".*aifb.uni-karlsruhe.de",
			"path" : "/Publikationen/showPublikationen.*"
		},
		{
			"host" : ".*aifb.uni-karlsruhe.de",
			"path" : "/Forschungsgruppen/.*"
		},
		{
			"host" : ".*ubka.uni-karlsruhe.de",
			"path" : "null"
		},
		{
			"host" : "arxiv.org",
			"path" : "null"
		},
		{
			"host" : ".*ingentaconnect.com",
			"path" : "null"
		},
		{
			"host" : ".*librarything\..*",
			"path" : "null"
		},
		{
			"host" : ".*adsabs.harvard.edu",
			"path" : "null"
		},
		{
			"host" : ".*aip.org",
			"path" : "null"
		},
		{
			"host" : "^.*ams.org",
			"path" : "/mathscinet.*"
		},
		{
			"host" : ".*interscience.wiley.com",
			"path" : "null"
		},
		{
			"host" : ".*iop.org",
			"path" : "/EJ.*"
		},
		{
			"host" : ".*prola.aps.org",
			"path" : "null"
		},
		{
			"host" : ".*bibsonomy.org",
			"path" : "/bib/bibtex.*"
		},
		{
			"host" : ".*bibsonomy.org",
			"path" : "/bibtex.*"
		},
		{
			"host" : ".*csdl2.computer.org",
			"path" : "null"
		},
		{
			"host" : ".*computer.org",
			"path" : "null"
		},
		{
			"host" : ".*amazon.ca",
			"path" : "null"
		},
		{
			"host" : ".*amazon.co.jp",
			"path" : "null"
		},
		{
			"host" : ".*amazon.co.uk",
			"path" : "null"
		},
		{
			"host" : ".*amazon.com",
			"path" : "null"
		},
		{
			"host" : ".*amazon.de",
			"path" : "null"
		},
		{
			"host" : ".*amazon.fr",
			"path" : "null"
		},
		{
			"host" : ".*plosjournals.org",
			"path" : "null"
		},
		{
			"host" : ".*nature.com",
			"path" : "null"
		},
		{
			"host" : ".*blackwell-synergy.com",
			"path" : "null"
		},
		{
			"host" : ".*dblp.uni-trier.de",
			"path" : "null"
		},
		{
			"host" : ".*search.mpi-inf.mpg.de",
			"path" : "/dblp/.*"
		},
		{
			"host" : ".*biomedcentral.com",
			"path" : "null"
		},
		{
			"host" : ".*worldcat.org",
			"path" : "/oclc/"
		},
		{
			"host" : ".*springer.com$",
			"path" : "null"
		},
		{
			"host" : ".*pubs.acs.org",
			"path" : "/action/downloadCitation.*"
		},
		{
			"host" : ".*pubs.acs.org",
			"path" : "/doi/abs/.*"
		},
		{
			"host" : ".*anthrosource.net",
			"path" : "/doi/abs/.*"
		},
		{
			"host" : ".*anthrosource.net",
			"path" : "/action/showCitFormats.*"
		},
		{
			"host" : ".*bmj.com",
			"path" : "null"
		},
		{
			"host" : ".*editlib.org",
			"path" : "/index.cfm.*"
		},
		{
			"host" : ".*informaworld.com",
			"path" : "null"
		},
		{
			"host" : ".*journals.cambridge.org",
			"path" : "/action/displayAbstract.*"
		},
		{
			"host" : ".*isrl.uiuc.edu",
			"path" : "null"
		},
		{
			"host" : ".*liebertonline.com",
			"path" : "/doi/abs/.*"
		},
		{
			"host" : ".*liebertonline.com",
			"path" : "/action/showCitFormats.*"
		},
		{
			"host" : ".*www.nber.org",
			"path" : "null"
		},
		{
			"host" : ".*usenix.org",
			"path" : "/events/.*"
		},
		{
			"host" : ".*usenix.org",
			"path" : "/publications/library/proceedings/.*\.html"
		},
		{
			"host" : ".*iucr.org",
			"path" : "null"
		},
		{
			"host" : ".*opticsinfobase.org",
			"path" : "null"
		},
		{
			"host" : ".*psycontent.metapress.com",
			"path" : "null"
		},
		{
			"host" : ".*journals.royalsociety.org",
			"path" : "null"
		},
		{
			"host" : ".*jstor.org",
			"path" : "/pss/.*"
		},
		{
			"host" : ".*jstor.org",
			"path" : "/action/exportSingleCitation.*"
		},
		{
			"host" : ".*eric.ed.gov",
			"path" : "null"
		},
		{
			"host" : ".*iwaponline.com",
			"path" : "null"
		},
		{
			"host" : ".*jmlr.csail.mit.edu",
			"path" : "null"
		},
		{
			"host" : ".*aclweb.org",
			"path" : "^/anthology-new.*\.pdf$"
		},
		{
			"host" : ".*arjournals.annualreviews.org",
			"path" : "null"
		},
		{
			"host" : ".*muse.jhu.edu",
			"path" : "null"
		},
		{
			"host" : ".*ssrn.com",
			"path" : "null"
		},
		{
			"host" : ".*scopus.com$",
			"path" : "null"
		},
		{
			"host" : ".*metapress.com",
			"path" : "null"
		},
		{
			"host" : ".*citeseerx.ist.psu.edu",
			"path" : "null"
		},
		{
			"host" : ".*openrepository.com",
			"path" : "null"
		},
		{
			"host" : ".*e-space.mmu.ac.uk",
			"path" : "/e-space.*"
		},
		{
			"host" : ".*eric.exeter.ac.uk",
			"path" : "/exeter.*"
		},
		{
			"host" : ".*arrts.gtcni.org.uk",
			"path" : "/gtcni.*"
		},
		{
			"host" : ".*hirsla.lsh.is",
			"path" : "/lsh.*"
		},
		{
			"host" : ".*pion.co.uk",
			"path" : "null"
		},
		{
			"host" : ".*citeulike.org",
			"path" : "null"
		},
		{
			"host" : ".*ams.allenpress.com",
			"path" : "null"
		},
		{
			"host" : ".*liinwww.ira.uka.de",
			"path" : "/cgi-bin/bibshow.*"
		},
		{
			"host" : ".*wormbase.org",
			"path" : "null"
		},
		{
			"host" : ".*scholar.google..*",
			"path" : "/scholar.bib.*"
		},
		{
			"host" : ".*dlib.org$",
			"path" : "null"
		},
		{
			"host" : ".*scientificcommons.org",
			"path" : "null"
		}
	]
}