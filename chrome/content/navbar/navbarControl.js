var navbarControl = {
	bookmarkService: null,
	bookmarkdb: null,
	
	tags: {
		bookmark: {img: "icons/bluestar.png", description: "bibsonomy bookmark"},
		bibtex: {img: "icons/bluebibtex.png", description: "bibsonomy bibtex"},
	},
	
	observer: {
		onBeginUpdateBatch: function() {},
		onEndUpdateBatch: function() {},
		onItemVisited: function(id, visitID, time) {},
		onItemMoved: function(id, oldParent, oldIndex, newParent, newIndex) {},
		onItemAdded: function(id, folder, index) {
			navbarControl.onUpdate();
		},
		onItemRemoved: function(id, folder, index) {
			navbarControl.onUpdate();
		},
		onItemChanged: function(id, property, isAnnotationProperty, value) {
			navbarControl.onUpdate();
		},
		QueryInterface: function(iid) {
			if (iid.equals(Components.interfaces.nsINavBookmarkObserver) ||
				iid.equals(Components.interfaces.nsISupports)) {
			  return this;
			}
			throw Cr.NS_ERROR_NO_INTERFACE;
		}
	},

	listener: {
		onStateChange: function() {},
		onStatusChange: function() {},
		onProgressChange: function() {},
		onSecurityChange: function() {},
		onLinkIconAvailable: function() {},

		onLocationChange: function(aProgress, aRequest, aURI) {
			navbarControl.onSwitch();
		},

		QueryInterface: function(iid) {
			if (iid.equals(Components.interfaces.nsIWebProgressListener) ||
			   iid.equals(Components.interfaces.nsISupportsWeakReference) ||
			   iid.equals(Components.interfaces.nsISupports))
			 return this;
			throw Components.results.NS_NOINTERFACE;
		}
	
	},


	/*
	 * INIT function
	 */
	init: function () {
		// remove star button
		if(Preferences.isStarButton()){
			starButton = document.getElementById('star-button');
			starButton.setAttribute("style","")
		} else {
			starButton = document.getElementById('star-button');
			starButton.setAttribute("style","display: none !important;")
		}
	
		this.bookmarkService = Components
			.classes["@mozilla.org/browser/nav-bookmarks-service;1"]
			.getService(Components.interfaces.nsINavBookmarksService);

		this.bookmarkService.addObserver(this.observer, false);

		this.bookmarkdb = new BookmarkDB;
		
		gBrowser.addProgressListener(this.listener, Components.interfaces.nsIWebProgress.NOTIFY_STATE_ALL);

		var navbarbox = document.getElementById("navbar-main-box");

		var i, count = 0;
		for (i in this.tags) {
			image = document.createElement("image");
			image.setAttribute("id", "navbar-item-" + i);
			image.setAttribute("class", "navbar-icon");
			image.setAttribute("tagged", "false");
			image.setAttribute("tagname", i);
			image.setAttribute("tooltiptext", this.tags[i].description);
			image.style.backgroundImage = "url(chrome://bibsonomy/skin/navbar/" + this.tags[i].img + ")";
			image.addEventListener("click", function(obj) { navbarControl.handleClick(obj); }, false);

			if (Math.random() > 0.5) { // <<-- wtf??
				image.setAttribute("picked", "true");
			}

			navbarbox.appendChild(image);
			
		}
		this.onUpdate();

	},

	uninit: function() {
		this.bookmarkService.removeObserver(this.observer);
	},

	/*
	 * Click on icon
	 */

	handleClick: function(event) {
		var obj = event.originalTarget;
		var uri = gBrowser.selectedBrowser.currentURI;
		var tag = obj.getAttribute("tagname");

		if(tag == 'bookmark'){
			// open create/edit bookmark menu
			var features = "chrome,titlebar,toolbar,centerscreen,modal";
			window.openDialog("chrome://bibsonomy/content/postBookmark.xul", "post bookmark",
				features,
				uri.spec,
				document.title,
				getBrowser().selectedBrowser.contentDocument.getSelection());
		}
		if(tag == 'bibtex'){
			// forward to bibsonomy bibtex-post site
			var mainWindow = window.QueryInterface(Components.interfaces.nsIInterfaceRequestor)
				.getInterface(Components.interfaces.nsIWebNavigation)
				.QueryInterface(Components.interfaces.nsIDocShellTreeItem)
				.rootTreeItem
				.QueryInterface(Components.interfaces.nsIInterfaceRequestor)
				.getInterface(Components.interfaces.nsIDOMWindow);
			mainWindow.getBrowser().contentWindow.document.location.href='http://www.bibsonomy.org/BibtexHandler?requTask=upload&url='+encodeURIComponent(uri.spec)+'&description='+encodeURIComponent(document.title)+'&selection='+encodeURIComponent(window.getSelection());
		}
	},

	showAll: function() {
		document.getElementById("navbar-main-box").setAttribute("class", "showall");
	},

	dontShowAll: function() {
		document.getElementById("navbar-main-box").setAttribute("class", "");
	},
	 
	/*
	 * Change in bookmark, observed by observer -check if the url has been
	 * posted already and mark it -check if it is possible to scrap the info for
	 * the site with bibtex
	 */
	onUpdate: function () {
		var uri = gBrowser.selectedBrowser.currentURI;
		var i, obj;

		for (i in this.tags) {
			obj = document.getElementById("navbar-item-" + i);
			obj.setAttribute("tagged", "false");
			tag = obj.getAttribute("tagname");

			if(tag == 'bookmark'){
				var bookmarkid = this.bookmarkdb.getBookmarkId(uri.spec);
				if(bookmarkid !== null){
					obj.setAttribute('tagged', 'true');
				} else {
					obj.setAttribute('tagged', 'false');
				}
			}

			if(tag == 'bibtex'){
				var bibtexsites = this.getBibtexsites();
				try{
					obj.setAttribute("style","display: none !important;");
					for(var j = 0; j < bibtexsites.patterns.length; j++){
						if(uri.host.search(bibtexsites.patterns[j].host) != -1){
							if(uri.path.search(bibtexsites.patterns[j].path) != -1 || bibtexsites.patterns[j].path == 'null'){
								obj.setAttribute('tagged', 'true');
								obj.setAttribute("style","background-image: url(chrome://bibsonomy/skin/navbar/" + this.tags[i].img + ");");
								return;
							}
						}
					}
				} catch (err) {
					// nothing
				}
			}
		}
	},


	onSwitch: function() {
		this.onUpdate();
	},
	
	getBibtexsites: function() {
		/*
		 * an object is created containing a single member "bindings", which
		 * contains an array containing three objects, each containing
		 * "ircEvent", "method", and "regex" members.
		 * 
		 * Members can be retrieved using dot or subscript operators.
		 * myJSONObject.patterns[0].host // ".*citebase.org"
		 * myJSONObject.patterns[0].path // "null"
		 * 
		 * eval()
		 */	
		// http://scraper.bibsonomy.org/service?action=info&format=json
		return eval('(' + this.bookmarkdb.getScraperInfo() + ')');
	}
};

window.addEventListener("load", function() {navbarControl.init();}, false);
window.addEventListener("unload", function() {navbarControl.uninit();}, false);