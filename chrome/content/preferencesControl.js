// opens preferenceswindow
function preferences() {
	var features = "chrome,titlebar,toolbar,centerscreen,modal";
	window.openDialog("chrome://bibsonomy/content/preferences.xul", "Preferences",features);
}

function init() {
	document.getElementById('textUsername').setAttribute('value', Preferences.getUser());
	document.getElementById('textApikey').setAttribute('value', Preferences.getApikey());
	document.getElementById('textServer').setAttribute('value', Preferences.getServer());
	document.getElementById('textLink').setAttribute('href', Preferences.getServer() + '/settings?seltab=2');
	document.getElementById('textBookmarklimit').setAttribute('value', Preferences.getSidebarBookmarkLimit());
		
/*	if(Preferences.isFirstImport() == "false"){
		if(Preferences.getDefaultFolderId() == -1){
			document.getElementById('menufolder').setAttribute('value', "Menu folder disabled");
		} else {
			document.getElementById('menufolder').setAttribute('value', Preferences.getDefaultFolder());
		}
		document.getElementById('menufolder').disabled = true;
		document.getElementById('cbFolder').disabled = true; 
	} else {
		document.getElementById('menufolder').setAttribute('value', Preferences.getDefaultFolder());
	}*/
	if(Preferences.isHomeButton() == 'true'){
		document.getElementById('homebuttonFalse').setAttribute('selected', 'false');
		document.getElementById('homebuttonTrue').setAttribute('selected', 'true');
	} else {
		document.getElementById('homebuttonFalse').setAttribute('selected', 'true');
		document.getElementById('homebuttonTrue').setAttribute('selected', 'false');
	}
	if(Preferences.isSidebarButton() == 'true'){
		document.getElementById('sidebarbuttonFalse').setAttribute('selected', 'false');
		document.getElementById('sidebarbuttonTrue').setAttribute('selected', 'true');
	} else {
		document.getElementById('sidebarbuttonFalse').setAttribute('selected', 'true');
		document.getElementById('sidebarbuttonTrue').setAttribute('selected', 'false');
	}
	if(Preferences.isStarButton() == true){
		document.getElementById('starbuttonFalse').setAttribute('selected', 'false');
		document.getElementById('starbuttonTrue').setAttribute('selected', 'true');
	} else {
		document.getElementById('starbuttonFalse').setAttribute('selected', 'true');
		document.getElementById('starbuttonTrue').setAttribute('selected', 'false');
	}
	if(Preferences.isNewtab() == true){
		document.getElementById('activetab').setAttribute('selected', 'false');
		document.getElementById('newtab').setAttribute('selected', 'true');
	} else {
		document.getElementById('activetab').setAttribute('selected', 'true');
		document.getElementById('newtab').setAttribute('selected', 'false');
	}

	var button = document.getElementById("okbutton");
	button.addEventListener('command',buttonPressed,true);
	var button = document.getElementById("deletebutton");
	button.addEventListener('command',buttonPressed,true);
}

function save() {
	var user = document.getElementById('textUsername').value;
	var apikey = document.getElementById('textApikey').value;
	Preferences.setUserApi(user, apikey);
	
	var server = document.getElementById('textServer').value;
	if(server.charAt(server.length-1) != '/'){
		server = server + '/';
	}
	
	Preferences.setServer(server);
	Preferences.setSidebarBookmarkLimit(document.getElementById('textBookmarklimit').value);
	
	if(document.getElementById('homebuttonTrue').getAttribute('selected') == 'true'){
		Preferences.setHomeButton('true');
	}
	if(document.getElementById('homebuttonFalse').getAttribute('selected') == 'true'){
		Preferences.setHomeButton('false');		
	}
	if(document.getElementById('sidebarbuttonTrue').getAttribute('selected') == 'true'){
		Preferences.setSidebarButton('true');
	}
	if(document.getElementById('sidebarbuttonFalse').getAttribute('selected') == 'true'){
		Preferences.setSidebarButton('false');		
	}
	if(document.getElementById('starbuttonTrue').getAttribute('selected') == 'true'){
		Preferences.setStarButton(true);
	}
	if(document.getElementById('starbuttonFalse').getAttribute('selected') == 'true'){
		Preferences.setStarButton(false);		
	}
	if(document.getElementById('newtab').getAttribute('selected') == 'true'){
		Preferences.setNewtab(true);
	}
	if(document.getElementById('activetab').getAttribute('selected') == 'true'){
		Preferences.setNewtab(false);		
	}

	/*	if(Preferences.isFirstImport() != "false"){
		if(document.getElementById('cbFolder').checked == true){
			Preferences.setDefaultFolder(document.getElementById('menufolder').value);
		} else {
			var bookmarks = Components.classes["@mozilla.org/browser/nav-bookmarks-service;1"]
									.getService(Components.interfaces.nsINavBookmarksService);
			Preferences.setDefaultFolderId(bookmarks.unfiledBookmarksFolder);
		}
	}*/	
}

//import
function buttonPressed(event){
	if(event.currentTarget.id == "okbutton"){
		save();
		window.close();
		if (document.getElementById('chooseTreeBookmark').value == 1){ // server import
			/*if(Preferences.isFirstImport() != "false"){
				if(document.getElementById('cbFolder').checked == true){
					Preferences.setDefaultFolder(document.getElementById('menufolder').value);
				} else {
					Preferences.setDefaultFolder(-1);
				}
			}*/
			var features = "chrome,titlebar,toolbar,centerscreen,modal";
			window.openDialog("chrome://bibsonomy/content/statusbar.xul", "Import bookmarks", features);
		} else 	if(document.getElementById('chooseTreeBookmark').value == 2){ // firefox import
			var features = "chrome,titlebar,toolbar,centerscreen,modal";
			window.openDialog("chrome://bibsonomy/content/firefoxImport.xul", "Import bookmarks", features );
		}
	}
	if(event.currentTarget.id == "deletebutton"){
		if(confirm("Are you sure you want to remove your bibsonomy bookmarks from firefox?")){
			bookmarkdb = new BookmarkDB();
//			if(document.getElementById('cbPlaces')){
				bookmarkdb.removeBookmarksFromPlaces();
//			}
		}
	}
	if(event.currentTarget.id == "advancedBookmarkOverview"){
		window.close();
		var features = "menubar=no,location=yes,resizable=yes,scrollbars=yes,status=yes";
		window.openDialog(	"chrome://bibsonomy/content/advancedBookmarkOverview.xul", 
							"Advanced Bookmark Overview",
							features);
	}
}