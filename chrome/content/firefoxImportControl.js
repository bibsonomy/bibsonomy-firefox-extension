function firefoxImport(){
	var folders = new Array();
	var foldersName = new Array();
	var bookmarks = Components.classes["@mozilla.org/browser/nav-bookmarks-service;1"]
			.getService(Components.interfaces.nsINavBookmarksService);

	if(document.getElementById("importMenu").checked){
		folders.push(bookmarks.bookmarksMenuFolder);
		foldersName.push("menu_folder");
	}
	if(document.getElementById("importToolbar").checked){
		folders.push(bookmarks.toolbarFolder);
		foldersName.push("toolbar_folder");
	}

	for(var i = 0; i < folders.length; i++){
		parseFolder([folders[i]], [foldersName[i]]);
	}
}

function cancel(){
	window.close();
}
	
function parseFolder(folder, parentFolders){
	var bookmarks = Components.classes["@mozilla.org/browser/nav-bookmarks-service;1"]
			.getService(Components.interfaces.nsINavBookmarksService);
	var history = Components.classes["@mozilla.org/browser/nav-history-service;1"]
			.getService(Components.interfaces.nsINavHistoryService);
	var query = history.getNewQuery();
	query.setFolders(folder, 1);

	var result = history.executeQuery(query, history.getNewQueryOptions());

	// The root property of a query result is an object representing the folder
	// you specified above.
	var folderNode = result.root;

	// Open the folder, and iterate over it's contents.
	folderNode.containerOpen = true;
	for ( var i = 0; i < folderNode.childCount; ++i) {
		var childNode = folderNode.getChild(i);

		// Some item properties.
		var title = childNode.title;
		var id = childNode.itemId;
		var type = childNode.type;

		// Some type-specific actions.
		if (type == Components.interfaces.nsINavHistoryResultNode.RESULT_TYPE_URI) {
			var parentFolderTitle = bookmarks.getItemTitle(bookmarks.getFolderIdForItem(id));
			var uri = childNode.uri;
			// now add the bookmark to the database with information title, id, uri and the parentfolders as tags
			bookmarkdb = new BookmarkDB();
//			alert("BOOKMARK in " + parentFolders + "\n" + bookmarkdb.isBookmarked(uri) + "\ntitle: " + title + "\nid: " + id + "\nuri: " + uri	+ "\ntype: " + type);
			if(bookmarkdb.isBookmarked(uri) === false){
				//title, tags, description, group, postUrl, checkBookmark, editUrl
				bookmarkdb.postBookmark(title, parentFolders, "", "private", uri, null, uri);
			} else {
		//		alert("bookmark already stored on the server!");
			}
		} else if (type == Components.interfaces.nsINavHistoryResultNode.RESULT_TYPE_FOLDER) {

			childNode.QueryInterface(Components.interfaces.nsINavHistoryContainerResultNode);
			childNode.containerOpen = true;
			// now you can iterate over a subfolder's children
//			alert("FOLDER in " + parentFolders + "\ntitle: " + title + "\nid: " + id + "\ntype: " + childNode.type);
			title = title.replace(/\s/g,'_');
			parentFolders.push(title);
			parseFolder([id], parentFolders);
			childNode.containerOpen = false;
		}
	}
	alert("Firefox bookmarks imported");
	window.close();
}