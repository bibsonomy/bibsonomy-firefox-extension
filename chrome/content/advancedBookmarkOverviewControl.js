var bookmarkdb = new BookmarkDB();
function init(){
	
	ownBookmarks();
	placesBookmarks();
	ownTags();
}

function ownBookmarks(){
	var bookmarks = bookmarkdb.readOwnBookmarks();
	
	for(var h = 0; h < bookmarks[bookmarks.length-1].length; h++){
		newTreecol('treecols', bookmarks[bookmarks.length-1][h]);
	}
	
	for(var i = 0; i < bookmarks.length - 1; i++) {
		var singleBookmark = new Array();
		for(var j = 0; j < bookmarks[i].length; j++) {
			singleBookmark.push(bookmarks[i][j]);
		}
		newTreeitem('treechildren', singleBookmark)
	}
}

function placesBookmarks(){
	var bookmarks = bookmarkdb.readPlaces();
	
	for(var h = 0; h < bookmarks[0].length; h++){
		newTreecol('placesTreecols', bookmarks[0][h]);
	}

	for(var i = 1; i < bookmarks.length; i++) {
		var singleBookmark = new Array();
		for(var j = 0; j < bookmarks[i].length; j++) {
			singleBookmark.push(bookmarks[i][j]);
		}
		newTreeitem('placesTreechildren', singleBookmark)
	}
}

function ownTags(){
	var bookmarks = bookmarkdb.readOwnTags();
	
	for(var h = 0; h < bookmarks[bookmarks.length-1].length; h++){
		newTreecol('tagsTreecols', bookmarks[bookmarks.length-1][h]);
	}

	for(var i = 0; i < bookmarks.length - 1; i++) {
		var singleBookmark = new Array();
		for(var j = 0; j < bookmarks[i].length; j++) {
			singleBookmark.push(bookmarks[i][j]);
		}
		newTreeitem('tagsTreechildren', singleBookmark)
	}
}

function newTreecol(colName ,name){
	var treecol = document.createElement('treecol'); 
	treecol.setAttribute('flex', '1');
	treecol.setAttribute('label', name);
	treecol.setAttribute('sortActive', true);
	
	if(name == 'id')
		treecol.setAttribute('sortDirection', 'ascending');
//	<splitter class="tree-splitter"/>
	var splitter = document.createElement('splitter');
	splitter.setAttribute('class', 'tree-splitter');
	document.getElementById(colName).appendChild(treecol);
	document.getElementById(colName).appendChild(splitter);
}

function newTreeitem(childrenName,bookmark) {
	var treeitem = document.createElement('treeitem');
	var treerow = document.createElement('treerow');
	for(var i = 0; i < bookmark.length; i++){
		var treecell = document.createElement('treecell');
		treecell.setAttribute('label', bookmark[i]);
		treerow.appendChild(treecell);
	}
	document.getElementById(childrenName).appendChild(treeitem).appendChild(treerow);
}
