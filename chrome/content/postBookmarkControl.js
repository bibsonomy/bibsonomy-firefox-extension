function postBookmark() {
	
	// somehow firefox adds a string " - Mozilla Firefox" (18) to the document.title,
	// and since we don't want to end every bookmark title with it we remove it here
	var documentTitle = document.title;
	if(documentTitle.substr(documentTitleEnd, documentTitle.length) == ' - Mozilla Firefox'){
		var documentTitleEnd = documentTitle.length - 18;
		documentTitle = documentTitle.substr(0, documentTitleEnd);
	}
	
	// loads the 'post bookmark' window with parameters such as current url, title of 
	// it and the selected text which will be the description
	var features = "chrome,titlebar,toolbar,centerscreen,modal";
	try{
		var selection = getBrowser().selectedBrowser.contentDocument.getSelection();
	} catch (err) {
		var selection = '';
	}
	window.openDialog("chrome://bibsonomy/content/postBookmark.xul", "post bookmark",
		features,
		getBrowser().selectedBrowser.contentDocument.location.href,
		documentTitle,
		selection);
	var sidebarWindow = document.getElementById("sidebar").contentWindow;
	// Verify that our sidebar is open at this moment:
	if (sidebarWindow.location.href == "chrome://bibsonomy/content/sidebar/sidebar.xul") {
		sidebarWindow.document.getElementById('taglistTabpanel').setAttribute('value','true');
		sidebarWindow.document.getElementById('tagcloudTabpanel').setAttribute('value','true');
		sidebarWindow.updateBookmarks();
		sidebarWindow.updateTags();
	} 
}

var bookmark;
var bookmarkdb;

function init(){
	// add buttons to event listener
	var button = document.getElementById("savebutton");
	button.addEventListener('command',saveButtonPressed,true);
	var button = document.getElementById("cancelbutton");
	button.addEventListener('command',cancelButtonPressed,true);

	bookmarkdb = new BookmarkDB();
	if(addUserGroups()===-1){ return -1;}

	
	if(addRecommendation(window.arguments[0])===-1) return -1;

	// set current url, title of url and selected text as description as start value
	var caption = window.arguments[0].length > 50 ? window.arguments[0].substring(0,60)+"..." : window.arguments[0];
	document.getElementById('caption').setAttribute('label', "post bookmark: "+caption);
	document.getElementById('textUrl').setAttribute('value', window.arguments[0]);
	document.getElementById('textTitle').setAttribute('value', window.arguments[1]);
	document.getElementById('textDescription').setAttribute('value', window.arguments[2]);
	document.getElementById('textTags').focus();

	bookmark = checkUrl();
	if(bookmark == -1){
		window.close();
		return;
	}
}

function addUserGroups(){
	groups = bookmarkdb.getGroups();
	if(groups===-1){
		return -1;
	}	
	for(var i = 0; i < groups.length; i++){
		var listitem = document.createElement('listitem');
		listitem.setAttribute('label', groups[i]);
		listitem.setAttribute('value', groups[i]);
		document.getElementById('grouplist').appendChild(listitem);
	}
}

function saveButtonPressed(){
	// make sure that it has at least one tag
	if(document.getElementById('textTags').value == ''){
		alert("You must enter at least one tag!");
	} else {
		// parse the tags string to an array, space is used as separator
		var stringToSplit = document.getElementById('textTags').value;
		var tags = new Array();
		tags = trim(stringToSplit).split(" ");

		// get groups
		if(document.getElementById('groupOther').selected){
			var xulGroups = document.getElementById('grouplist').selectedItems;
			var groups = new Array();
			for(var i = 0; i < xulGroups.length; i++){
				groups.push(xulGroups[i].value);
			}
		} else {
			var groups = new Array(document.getElementById('group').selectedItem.value);
		}

		// post bookmarks with the values from the post bookmark window
		var isBookmarked = bookmarkdb.isBookmarked(window.arguments[0]);
		var editUrl = null;
		if(isBookmarked === false){
			editUrl = document.getElementById('textUrl').value;
		} else if (isBookmarked === -1){
			return -1;
		} else {
			editUrl = window.arguments[0];
		}
		var success = bookmarkdb.postBookmark(
		/* Title 				*/ document.getElementById('textTitle').value,
		/* Tags 				*/ tags,
		/* Description 			*/ document.getElementById('textDescription').value,
		/* Group 				*/ groups,
		/*	Url					*/ document.getElementById('textUrl').value,
		/* 	aaaaaa				*/ bookmark,
		/*  bbbbbb				*/ editUrl);
		if(success === true){
			window.close();
		} else {
			if(confirm("Failed to create/edit bookmark, try again?")){
				checkUrl();
			} else {
				window.close();
			}
		}
	}
}

function cancelButtonPressed(){
	window.close();
}

// check if a url is already posted on the server
// if so load the already posted details and inform user about it
function checkUrl(){
	var url = document.getElementById('textUrl').value;
	var infolabel =  document.getElementById('info');

	// check if bookmark is posted already. Returns false if not posted, the bookmark with details if already posted
	bookmark = this.bookmarkdb.isBookmarked(url);
	if(bookmark == -1){
		return -1;
	} else if(bookmark != false){
		infolabel.setAttribute('value',"URL is already bookmarked, this will change your stored bookmark");
		
		// read the posted details and display it in the post bookmark window
		var tagsArray = new Array ();
		var tagsString = ""; 
		for(var j = 0; bookmark.getElementsByTagName('tag').length > j; j++){
			tagsArray.push(bookmark.getElementsByTagName('tag')[j].getAttribute('name'));
			if(tagsString === ""){
				tagsString = bookmark.getElementsByTagName('tag')[j].getAttribute('name');
			} else {
				tagsString = tagsString + " " + bookmark.getElementsByTagName('tag')[j].getAttribute('name');				
			}
		}
		var group = bookmark.getElementsByTagName('group')[0].getAttribute('name');
		if(group == 'public'){
			document.getElementById('groupPublic').setAttribute('selected', true);
		} else if(group == 'private'){
			document.getElementById('groupPublic').setAttribute('selected', false);
			document.getElementById('groupPrivate').setAttribute('selected', true);
		} else if (group == 'other'){ // TODO
			document.getElementById('groupPublic').setAttribute('selected', false);
			document.getElementById('groupOther').setAttribute('selected', true);
			document.getElementById('grouplist').setAttribute('disabled', false);
		}
		
		var description = bookmark.getElementsByTagName('post')[0].getAttribute('description');
		var title = bookmark.getElementsByTagName('bookmark')[0].getAttribute('title');
		document.getElementById('textTitle').setAttribute('value', title);
		document.getElementById('textDescription').setAttribute('value', description);
		document.getElementById('textTags').setAttribute('value', tagsString);
	} else if ((this.bookmarkdb.isBookmarked(window.arguments[0]) != false) && document.getElementById('textUrl').value != window.arguments[0]){
		infolabel.setAttribute('value',"url has been changed, the old url will be lost");
	} else {
		infolabel.setAttribute('value',"");
	}
	return bookmark;
}

function addRecommendation(urlToCheck){
	tagRecommendation = bookmarkdb.getTagRecommendation(urlToCheck,document);

	if(tagRecommendation===-1){
		return -1;
	}	
}

function addRecommendationtags(recommendation){
	recommendation = unescape(recommendation);
	var taglist = document.getElementById('textTags').value;
	var spos = taglist.indexOf(recommendation);
	if(spos == -1) { // recommendations isnt in the tag list yet -> add it
		taglist += " " + recommendation;
	} else { // recommenndation is already in the taglist -> remove it
		var ltaglist = taglist.slice(0, spos);
		var rtaglist = taglist.slice(spos+recommendation.length, taglist.length);
		taglist = ltaglist + rtaglist;
	}
	taglist = taglist.replace(/\s\s/g, " ");
	if(taglist.charAt(0) == ' '){
		taglist =  trim(taglist);
		taglist += " ";
	}
	document.getElementById('textTags').value = taglist;
}

function addSuggestion(newTag){
	newTag = unescape(newTag);
	taglist = document.getElementById('textTags').value;
	var array = taglist.split(" ");
	array.pop();
	array.push(newTag);
	document.getElementById('textTags').value = array.join(" ") + " ";
	addSuggestiontags(newTag);
}

function addSuggestiontags(string){
	limit = 1; // when to start to search for suggestions
	// split tags for suggestion
	var rstr = string.split(" ");
	if(rstr[rstr.length-1].length < limit){
		return;
	}
	
	// get tag suggestion
	suggestions = this.bookmarkdb.getTagSuggestions(rstr[rstr.length-1], 5);
	
	// check suggestions for tags which are already in the taglist
	var cleansuggestions = new Array();
	var show;
	for(var h = 0; h < suggestions.length; h++){
		show = true;
		for(var g = 0; g < rstr.length; g++){
			if(suggestions[h] == rstr[g]){
				show = false;
				break;
			}
		}
		if(show){
			cleansuggestions.push(suggestions[h]);
		}
	}
	
	// clear the suggestionlist
	rowSuggestions = document.getElementById('rowSuggestions');
	while (rowSuggestions.firstChild) {
		rowSuggestions.removeChild(rowSuggestions.firstChild);
	}
	
	// add new element to the suggestionlist
	for(var i = 0; i < cleansuggestions.length; i++){
		descr = document.createElement('description');
		descr.setAttribute('value', cleansuggestions[i]);
		descr.setAttribute('class', 'tagSuggestion');
		descr.setAttribute('onmouseup','addSuggestion("' + escape(cleansuggestions[i])+ '")');
		rowSuggestions.appendChild(descr);
	}
}

function trim (str) {
  // Erst führende, dann Abschließende Whitespaces entfernen
  // und das Ergebnis dieser Operationen zurückliefern
  return str.replace (/^\s+/, '').replace (/\s+$/, '');
}