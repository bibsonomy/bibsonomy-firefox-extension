var ToolbarControl = {
/* Remove the Bookmark star */
//#star-button {
//display: none !important; } 

	// open new site to the users bibsonomy site 
	bibsonomyHome: function() {
		if(Preferences.getUser() != '' && Preferences.getServer() != ''){
			var url = Preferences.getServer() + "user/" + Preferences.getUser();
			var mainWindow = window.QueryInterface(Components.interfaces.nsIInterfaceRequestor)
				.getInterface(Components.interfaces.nsIWebNavigation)
				.QueryInterface(Components.interfaces.nsIDocShellTreeItem)
				.rootTreeItem
				.QueryInterface(Components.interfaces.nsIInterfaceRequestor)
				.getInterface(Components.interfaces.nsIDOMWindow);
			mainWindow.getBrowser().contentWindow.document.location.href = url;
		} else {
			alert('set your preferences!');
		}
	},
	
	// opensidebar
	bibsonomySidebar: function() {
		document.getElementById('viewSidebarBibsonomy').doCommand();
	},
	
	// register buttons
	onLoadEvent: function()
	{
		var bibsonomyHome = document.getElementById('bibsonomyHome');
		var bibsonomySidebar = document.getElementById('bibsonomySidebar');
			
		var objToolbox = document.getElementById("navigator-toolbox");
		var strChild = '';
		var strSet = '';
		var objNavBar = false;
		
		if(objToolbox) {
			for (var i = 0; i < objToolbox.childNodes.length; ++i) {
				if(objToolbox.childNodes[i].id == "nav-bar" && objToolbox.childNodes[i].getAttribute("customizable") == "true") {
					objNavBar = objToolbox.childNodes[i];
					break;
				}
			}
		}
		if(objNavBar) {
			for (var i = 0; i < objNavBar.childNodes.length; i++) {
				if(objNavBar.childNodes[i].id == "urlbar-container") {
					if(!bibsonomyHome) {
						strSet += "bibsonomyHome,"							
					}
					if(!bibsonomySidebar) {
						strSet += "bibsonomySidebar,"														
					}
				}
				strSet += objNavBar.childNodes[i].id + ",";
			}
			if(Preferences.isHomeButton() != 'true'){
				strSet = strSet.replace(/bibsonomyHome,/, '');
			}
			if(Preferences.isSidebarButton() != 'true'){
				strSet = strSet.replace(/bibsonomySidebar,/, '');
			}
			strSet = strSet.substring(0, strSet.length-1);
			objNavBar.currentSet = strSet;
			objNavBar.setAttribute("currentset", strSet);
			objNavBar.ownerDocument.persist(objNavBar.id, "currentset");
			BrowserToolboxCustomizeDone(true);
		} 
		
		strSet = strSet.substring(0, strSet.length-1);
		toolbar.currentSet = strSet;
		
		if(toolbar.setAttribute) {
			toolbar.setAttribute("currentset", strSet);
		}
		BrowserToolboxCustomizeDone(true);
	}
}

window.addEventListener("load", function() 	{ ToolbarControl.onLoadEvent(); }, false);