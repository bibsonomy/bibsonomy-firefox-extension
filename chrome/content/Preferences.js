var Preferences = {
	
	setUserApi: function (user, apikey)  { 
		this.prefs = Components.classes["@mozilla.org/preferences-service;1"]
		                                .getService(Components.interfaces.nsIPrefService)
		                				.getBranch("extensions.bibsonomy.");
		this.prefs.setCharPref("username", user);
		this.prefs.setCharPref("apikey", apikey);
	},

	getUser: function(){
		this.prefs = Components.classes["@mozilla.org/preferences-service;1"]
		                                .getService(Components.interfaces.nsIPrefService)
		                				.getBranch("extensions.bibsonomy.");
		return this.prefs.getCharPref("username");
	},
	
	getApikey: function(){
		this.prefs = Components.classes["@mozilla.org/preferences-service;1"]
		                                .getService(Components.interfaces.nsIPrefService)
		                				.getBranch("extensions.bibsonomy.");
		return this.prefs.getCharPref("apikey");
	},
	
	getDefaultFolder: function(){
		this.prefs = Components.classes["@mozilla.org/preferences-service;1"]
		                                .getService(Components.interfaces.nsIPrefService)
		                				.getBranch("extensions.bibsonomy.");
		return this.prefs.getCharPref("defaultfolder");
	},

	setDefaultFolder: function(value){
		this.prefs = Components.classes["@mozilla.org/preferences-service;1"]
		                                .getService(Components.interfaces.nsIPrefService)
		                				.getBranch("extensions.bibsonomy.");
		return this.prefs.setCharPref("defaultfolder", value);		
	},
	
	getDefaultFolderId: function(){
		this.prefs = Components.classes["@mozilla.org/preferences-service;1"]
		                                .getService(Components.interfaces.nsIPrefService)
		                				.getBranch("extensions.bibsonomy.");
		var bookmarks = Components.classes["@mozilla.org/browser/nav-bookmarks-service;1"]
								.getService(Components.interfaces.nsINavBookmarksService);		
		return this.prefs.getCharPref("defaultfolderid");
	},

	setDefaultFolderId: function(value){
		this.prefs = Components.classes["@mozilla.org/preferences-service;1"]
		                                .getService(Components.interfaces.nsIPrefService)
		                				.getBranch("extensions.bibsonomy.");
		return this.prefs.setCharPref("defaultfolderid", value);		
	},
	
	getServer: function(){
		this.prefs = Components.classes["@mozilla.org/preferences-service;1"]
		                                .getService(Components.interfaces.nsIPrefService)
		                				.getBranch("extensions.bibsonomy.");
		return this.prefs.getCharPref("server");
	},
	
	setServer: function(server){
		this.prefs = Components.classes["@mozilla.org/preferences-service;1"]
		                                .getService(Components.interfaces.nsIPrefService)
		                				.getBranch("extensions.bibsonomy.");
		return this.prefs.setCharPref("server", server);
	},
	
	isFirstImport: function(){
		this.prefs = Components.classes["@mozilla.org/preferences-service;1"]
		                                .getService(Components.interfaces.nsIPrefService)
		                				.getBranch("extensions.bibsonomy.");
		return this.prefs.getCharPref("firstImport");
	},
	
	setFirstImport: function(firstImport){
		this.prefs = Components.classes["@mozilla.org/preferences-service;1"]
		                                .getService(Components.interfaces.nsIPrefService)
		                				.getBranch("extensions.bibsonomy.");
		return this.prefs.setCharPref("firstImport", firstImport);
	},
	
	isSidebarButton: function(){
		this.prefs = Components.classes["@mozilla.org/preferences-service;1"]
		                                .getService(Components.interfaces.nsIPrefService)
		                				.getBranch("extensions.bibsonomy.");
		return this.prefs.getCharPref("sidebarButton");		
	},
	
	isHomeButton: function(){
		this.prefs = Components.classes["@mozilla.org/preferences-service;1"]
		                                .getService(Components.interfaces.nsIPrefService)
		                				.getBranch("extensions.bibsonomy.");
		return this.prefs.getCharPref("homeButton");		
	},
	
	setSidebarButton: function(value){
		this.prefs = Components.classes["@mozilla.org/preferences-service;1"]
		                                .getService(Components.interfaces.nsIPrefService)
		                				.getBranch("extensions.bibsonomy.");
		return this.prefs.setCharPref("sidebarButton", value);		
	},
	
	setHomeButton: function(value){
		this.prefs = Components.classes["@mozilla.org/preferences-service;1"]
		                                .getService(Components.interfaces.nsIPrefService)
		                				.getBranch("extensions.bibsonomy.");
		return this.prefs.setCharPref("homeButton", value);		
	},
	
	setShowurl: function(value){
		this.prefs = Components.classes["@mozilla.org/preferences-service;1"]
		                                .getService(Components.interfaces.nsIPrefService)
		                				.getBranch("extensions.bibsonomy.");
		this.prefs.setCharPref("showurl", value);		
	},
	
	setTagview: function(value){
		this.prefs = Components.classes["@mozilla.org/preferences-service;1"]
		                                .getService(Components.interfaces.nsIPrefService)
		                				.getBranch("extensions.bibsonomy.");
		this.prefs.setCharPref("tagview", value);		
	},

	getShowurl: function(){
		this.prefs = Components.classes["@mozilla.org/preferences-service;1"]
		                                .getService(Components.interfaces.nsIPrefService)
		                				.getBranch("extensions.bibsonomy.");
		if(this.prefs.getCharPref("showurl") == "true")
			return true;
		else
			return false;
	},
	
	getTagview: function(){
		this.prefs = Components.classes["@mozilla.org/preferences-service;1"]
		                                .getService(Components.interfaces.nsIPrefService)
		                				.getBranch("extensions.bibsonomy.");
		return this.prefs.getCharPref("tagview");
	},
	
	isNewtab: function(){
		this.prefs = Components.classes["@mozilla.org/preferences-service;1"]
			                            .getService(Components.interfaces.nsIPrefService)
			              				.getBranch("extensions.bibsonomy.");
		if(this.prefs.getCharPref("newtab") == "true")
			return true;
		else
			return false;
	},
	
	setNewtab: function(value) {
		this.prefs = Components.classes["@mozilla.org/preferences-service;1"]
		                                .getService(Components.interfaces.nsIPrefService)
		                				.getBranch("extensions.bibsonomy.");
		this.prefs.setCharPref("newtab", value);
	},
	
	setStarButton: function (value) {
		this.prefs = Components.classes["@mozilla.org/preferences-service;1"]
		                                .getService(Components.interfaces.nsIPrefService)
		                				.getBranch("extensions.bibsonomy.");
		this.prefs.setCharPref("starbutton", value);
	},
	
	isStarButton: function(){
		this.prefs = Components.classes["@mozilla.org/preferences-service;1"]
		                                .getService(Components.interfaces.nsIPrefService)
		                				.getBranch("extensions.bibsonomy.");
		if(this.prefs.getCharPref("starbutton") == "true")
			return true;
		else
			return false;
	},

	setRelationDate: function (date)  { 
		this.prefs = Components.classes["@mozilla.org/preferences-service;1"]
		                                .getService(Components.interfaces.nsIPrefService)
		                				.getBranch("extensions.bibsonomy.");
		this.prefs.setCharPref("relation", date);
	},

	getRelationDate: function(){
		this.prefs = Components.classes["@mozilla.org/preferences-service;1"]
		                                .getService(Components.interfaces.nsIPrefService)
		                				.getBranch("extensions.bibsonomy.");
		return this.prefs.getCharPref("relation");
	},
	
	setScraperDate: function (date)  { 
		this.prefs = Components.classes["@mozilla.org/preferences-service;1"]
		                                .getService(Components.interfaces.nsIPrefService)
		                				.getBranch("extensions.bibsonomy.");
		this.prefs.setCharPref("scraperDate", date);
	},

	getScraperDate: function(){
		this.prefs = Components.classes["@mozilla.org/preferences-service;1"]
		                                .getService(Components.interfaces.nsIPrefService)
		                				.getBranch("extensions.bibsonomy.");
		return this.prefs.getCharPref("scraperDate");
	},
	
	setScraper: function (date)  { 
		this.prefs = Components.classes["@mozilla.org/preferences-service;1"]
		                                .getService(Components.interfaces.nsIPrefService)
		                				.getBranch("extensions.bibsonomy.");
		this.prefs.setCharPref("scraper", date);
	},

	getScraper: function(){
		this.prefs = Components.classes["@mozilla.org/preferences-service;1"]
		                                .getService(Components.interfaces.nsIPrefService)
		                				.getBranch("extensions.bibsonomy.");
		return this.prefs.getCharPref("scraper");
	},

	setSidebarBookmarkLimit: function (data)  { 
		this.prefs = Components.classes["@mozilla.org/preferences-service;1"]
		                                .getService(Components.interfaces.nsIPrefService)
		                				.getBranch("extensions.bibsonomy.");
		this.prefs.setIntPref("bookmarkDisplayLimit", data);
	},

	getSidebarBookmarkLimit: function(){
		this.prefs = Components.classes["@mozilla.org/preferences-service;1"]
		                                .getService(Components.interfaces.nsIPrefService)
		                				.getBranch("extensions.bibsonomy.");
		return this.prefs.getIntPref("bookmarkDisplayLimit");
	}
}