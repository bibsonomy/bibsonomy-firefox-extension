function BookmarkDB() {
	// firefox services
	var bookmarks = Components.classes["@mozilla.org/browser/nav-bookmarks-service;1"]
										.getService(Components.interfaces.nsINavBookmarksService);
	var tagsService = Components.classes["@mozilla.org/browser/tagging-service;1"]
										.getService(Components.interfaces.nsITaggingService);
	var storageService = Components.classes["@mozilla.org/storage/service;1"]
										.getService(Components.interfaces.mozIStorageService);
	var faviconService = Components.classes["@mozilla.org/browser/favicon-service;1"] 
										.getService(Components.interfaces.nsIFaviconService); 
	var ioService = Components.classes["@mozilla.org/network/io-service;1"]
										.getService(Components.interfaces.nsIIOService);
	var historyService = Components.classes["@mozilla.org/browser/nav-history-service;1"]
										.getService(Components.interfaces.nsINavHistoryService);

this.blub = function(){
//		var evt = document.createEvent();
	//	bookmarks.fireEvent("onItemAdded", evt);
}

	// set username/apikey/server
	var user = Preferences.getUser();
	var apikey = Preferences.getApikey();
	var server = Preferences.getServer();

	/* sqlite-database
	 * bibBookmarks table:
	 * 		-id
	 * 		-title
	 * 		-description
	 * 		-group_
	 * 		-postingdate
	 * 		-lastchangedate
	 * 		-hash
	 * 		-url
	 * bibTags table:
	 * 		-tags
	 * 		-id
	 */
	// get or create sqlite databasea and tables
	var file = Components.classes["@mozilla.org/file/directory_service;1"]
	                              .getService(Components.interfaces.nsIProperties)
	                              .get("ProfD", Components.interfaces.nsIFile);
	file.append("bibsonomyDB.sqlite");
	
	this.mDBConn = storageService.openDatabase(file);
	var dbName = "bibBookmarks";// + user;// + server;
	var dbNameTags = "bibTags";// + user;// + server;
	var dbNameRelations = "bibRelations";
	var dbNameCreateStatement = "CREATE TABLE " + dbName + " (id INTEGER PRIMARY KEY UNIQUE, title TEXT, description TEXT, group_ TEXT, postingdate TEXT, lastchangedate TEXT, hash TEXT UNIQUE, url TEXT UNIQUE)";
	var dbNameTagsCreateStatement = "CREATE TABLE " + dbNameTags + " (tag TEXT, id INTEGER, UNIQUE(tag,id), PRIMARY KEY(tag,id))";
	//CREATE TABLE bibBookmarks (id INTEGER PRIMARY KEY UNIQUE, title TEXT, description TEXT, group_ TEXT, postingdate TEXT, lastchangedate TEXT, hash TEXT UNIQUE, url TEXT UNIQUE)
	//CREATE TABLE bibTags (tag TEXT, id INTEGER, UNIQUE(tag,id), PRIMARY KEY(tag,id))

	// check if the databse has been modified
	var updateDb = false;
	if(this.mDBConn.tableExists(dbName)){
		var statement = this.mDBConn.createStatement(" SELECT * FROM SQLITE_MASTER where name=?1 or name=?2");
		statement.bindStringParameter(0,dbName);
		statement.bindStringParameter(1,dbNameTags);
		while(statement.executeStep()){
			if(statement.getString(4) != dbNameCreateStatement && statement.getString(4) != dbNameTagsCreateStatement){
				updateDb = true;
			}
		}		
		statement.reset();

		if(updateDb){
			try{ 
				statement = this.mDBConn.createStatement("create table temp as select id, hash, url from "+ dbName);
				statement.execute();
				statement.reset();
				statement = this.mDBConn.createStatement("drop table " + dbName);
				statement.execute();
				statement.reset();
				statement = this.mDBConn.createStatement("drop table " + dbNameTags);
				statement.execute();
				statement.reset();
			} catch (err) {
				alert("failed to update database\n" + err);
			} finally {
				statement.reset();
			}
		}
	}

	// create new database for bookmarks
	if(!this.mDBConn.tableExists(dbName)){
		try{
			this.mDBConn.createTable(dbName, "id INTEGER PRIMARY KEY UNIQUE, title TEXT, description TEXT, " + 
								"group_ TEXT, postingdate TEXT, lastchangedate TEXT, hash TEXT UNIQUE, url TEXT UNIQUE");
			var statement = this.mDBConn.createStatement("create index idxbibBookmarks on bibBookmarks (id);");
			statement.execute(); 
		} catch (err) {
			alert("failed to create an index!");
		} finally {
			statement.reset();
		}
	}
	// create new database for tags
	if(!this.mDBConn.tableExists(dbNameTags)){
		this.mDBConn.createTable(dbNameTags, "tag TEXT, id INTEGER, UNIQUE(tag,id), PRIMARY KEY(tag,id)");
		try{
			var statement = this.mDBConn.createStatement("create index idxbibtags on bibtags (id);");
			statement.execute(); 
		} catch (err) {
			alert("failed to create an index!");
		} finally {
			statement.reset();
		}
	}

	// create new database for relations
	if(!this.mDBConn.tableExists(dbNameRelations)){
		this.mDBConn.createTable(dbNameRelations, "supertag TEXT, subtags TEXT, UNIQUE(supertag), PRIMARY KEY(supertag)");
		try{
			var statement = this.mDBConn.createStatement("create index idxbibrelations on bibrelations (supertag);");
			statement.execute(); 
		} catch (err) {
			alert("failed to create an index!");
		} finally {
			statement.reset();
		}
	}

	// copy old ids/urls/hashs if the database has been modified
	if(updateDb){
		alert("The database has been changed and must be updated!\n This can take serval minutes.");
		try{
			var statement2 = this.mDBConn.createStatement("select * from temp");
			while(statement2.executeStep()){
				var statement3 = this.mDBConn.createStatement("INSERT INTO " + dbName + " values (?1, '-', '-', '-', '-', '-', ?2, ?3) ");
				statement3.bindInt32Parameter(0, statement2.getString(0));
				statement3.bindStringParameter(1, statement2.getString(1));
				statement3.bindStringParameter(2, statement2.getString(2));
				statement3.execute();
			}
			statement3.reset();
			statement3 = this.mDBConn.createStatement("drop table temp");
			statement3.execute();
		} catch (err) {
			alert("error while adding bookmark to database \n" + err);
		} finally {
			statement3.reset();
			statement2.reset();
		}
	}

	// add or update bookmark in own databases
	this.addToOwnDatabase = function(id, title, description, group, postingdate, lastchangedate, hash, url, tags){
		this.addToOwnTagDatabase(id, tags);
		try{
			var statement = this.mDBConn.createStatement("INSERT OR REPLACE INTO " + dbName + " VALUES(?1,?2,?3,?4,?5,?6,?7,?8)");
			statement.bindInt32Parameter(0, id);
			statement.bindStringParameter(1, title);
			statement.bindStringParameter(2, description);
			statement.bindStringParameter(3, group);
			statement.bindStringParameter(4, postingdate);
			statement.bindStringParameter(5, lastchangedate);
			statement.bindStringParameter(6, hash);
			statement.bindStringParameter(7, url);
			statement.execute();
		} catch (err) {
			alert("error while adding bookmark to database \n" + err);
		} finally {
			statement.reset();
		}
	};

	// remove bookmark from own database
	this.removeFromOwnDatabase = function(id){
		this.removeFromOwnTagDatabase(id);
		try{
			var statement = this.mDBConn.createStatement("DELETE FROM " + dbName + " WHERE id = (?1) ");
			statement.bindInt32Parameter(0, id);
			statement.execute();
		} catch (err) {
			alert("error while deleting from own database!\n" + err);
		} finally {
			statement.reset();
		}
	}

	// add tags and id to own tag database
	this.addToOwnTagDatabase = function(id, tags){
		try{
			var statement = this.mDBConn.createStatement("delete from " + dbNameTags + " where id=?1");
			statement.bindStringParameter(0, id);
			statement.execute();
			
			var statement = this.mDBConn.createStatement("INSERT OR REPLACE INTO " + dbNameTags + " VALUES(?1,?2)");
			for(var i = 0; i < tags.length; i++){
				statement.bindStringParameter(0, tags[i]);				
				statement.bindStringParameter(1, id);
				statement.execute();
			}
		} catch (err) {
			alert("error while checking a tag (owntagdb) \n" + err);
		} finally {
			statement.reset();
		}
	}

	// remove a bookmark from own tag database
	this.removeFromOwnTagDatabase = function(id){
		try{
			var statement = this.mDBConn.createStatement("DELETE FROM " + dbNameTags + " WHERE id=?1 ");
			statement.bindStringParameter(0, id);
			statement.execute();
		} catch (err) {
			alert("error while deleting from own tag database!\n" + err);
		} finally {
			statement.reset();
		}
	}

	// add bookmark to places-system
	this.addToPlaces = function(title, tags, postUrl, editUrl){
		var bookmarkId = this.getBookmarkId(editUrl);
		var bookmarkURI = '';

		try{
			if(bookmarkId !== null)
				bookmarkURI = bookmarks.getBookmarkURI(bookmarkId);
		} catch (err) { // if bookmark is deleted from places
			this.removeFromOwnDatabase(bookmarkId);
			bookmarkId = null;
		}

		
		if(bookmarkId === null){ // create a new bookmark if there is still no entry with that url in the local db 
			// new uri for palces
			bookmarkURI = ioService.newURI(postUrl, null, null);

			// add Url, Title, Folder to places
			bookmarkId = bookmarks.insertBookmark(
				this.getDefaultFolder(), // The id of the folder the bookmark will be placed in.
				bookmarkURI,             // The URI of the bookmark - an nsIURI object.
				bookmarks.DEFAULT_INDEX, // The position of the bookmark in it's parent folder.
				title);    // The title of the bookmark.

			// add tags to places
			tagsService.tagURI(bookmarkURI, tags);
		} else {
			if(postUrl != editUrl){ // change url for bookmark
				var uri = bookmarks.getBookmarkURI(bookmarkId);
				tagsService.untagURI(uri, this.getTags(bookmarkId), 1);
				bookmarkURI = ioService.newURI(postUrl, null, null);
					
				bookmarks.changeBookmarkURI( bookmarkId, bookmarkURI);
				tagsService.tagURI(bookmarkURI, tags);
				//bookmarks.removeItem(bookmarkId);
			}
			
			// set/update title
			if(title != this.getTitle(bookmarkId)){
				bookmarks.setItemTitle(bookmarkId, title);
			}
		
			// set/update tags
			var inDtTags = this.getTags(bookmarkId);
			var update = false;
			for(var i = 0; i < tags.length; i++){
				if(this.find(tags[i], inDtTags) === false){
					update = true;
				}
			}
			for(var i = 0; i < inDtTags.length; i++){
				if(this.find(inDtTags[i], tags) === false){
					update = true;
				}
			}
			if(update){
				tagsService.untagURI(bookmarkURI, inDtTags, 1); 
				tagsService.tagURI(bookmarkURI, tags);
			}
		}

		// add the favicon		
		var re = new RegExp('^(?:f|ht)tp(?:s)?\://([^/]+)', 'im');
        var topLevelDomain = "http://" + postUrl.match(re)[1].toString();
        var favicon = topLevelDomain + "/favicon.ico";
		var pageURI = ioService.newURI(topLevelDomain, null, null);
	
		var faviconURI = ioService.newURI(favicon, null, null);
		faviconService.setAndLoadFaviconForPage(bookmarkURI, faviconURI, false);

		return bookmarkId;
	}
	
	// remove bookmark from places
	this.removeFromPlaces = function(bookmarkId){
		try{
			var uri = bookmarks.getBookmarkURI(bookmarkId);
		} catch (err) {
			return;
		}
		
		var tags = this.getTags(bookmarkId);
		tagsService.untagURI(uri, tags, 1); 
		// delete bookmark from places
		bookmarks.removeItem(bookmarkId);
	}
	
	// create or update bookmark on the server
	this.addToBibsonomyServer = function(title, tags, description, group, postUrl, checkBookmark, editUrl){
		var update = false;

		// check if the bookmark is already bookmarked
		var bookmark = this.isBookmarked(editUrl);
		if(bookmark === -1){
			return -1;
		}
		if(bookmark != false){
			update = true;
		}

		// create xml document 
		var xmlDocument = this.buildXmlPostBookmarkDocument(description, user, tags, group, postUrl, title);
		var hash;
		if(update){ // update the bookmark on bibsonomy server
			// create md5 hash for url
			hash = this.md5(editUrl);

			var xmlHttp = new XMLHttpRequest();
			xmlHttp.open('PUT', server + "api/users/" + user + "/posts/" + hash, false, user, apikey);
		} else { // create new bookmark on bibsonomy server
			// post bookmark on bibsonomy server
			var xmlHttp = new XMLHttpRequest();
			xmlHttp.open('POST', server + "api/users/" + user + "/posts", false, user, apikey);
		}
		xmlHttp.setRequestHeader("Content-Type", "text/xml");
		try{
			xmlHttp.send(xmlDocument);
		} catch(err) {
			alert("Couldn't connect to the server: " + server);
			return -1;
		}

		/*	statuscodes
		 *  200 ok (put)
		 * 	201 created (post) 
		 * 	400 bad request
		 * 	401 Unauthorized
		 * 	403 Forbidden
		 * 	500 Internal Server Error
		 */
		var statuscode = xmlHttp.status;
		if(statuscode == 201 || statuscode == 200 ){
			var response = xmlHttp.responseXML;
			var navItems = response.getElementsByTagName('resourcehash');
			hash = navItems[0].firstChild.data
			return hash;
		} else {
			alert("error while adding bookmark (server) \nerror code: "+ statuscode+"\n-400 bad request \n-401 Unauthorized \n-403 Forbidden \n-500 Internal Server Error");
			return -1;
		}
	}
	
	// remove a bookmark from server
	this.removeFromBibsonomyServer = function(url){
		var hash = this.md5(url);
		var xmlHttp = new XMLHttpRequest();

		xmlHttp.open('DELETE', server + "api/users/" + user + "/posts/" + this.getHash(url), false, user, apikey);
		xmlHttp.setRequestHeader("Content-Type", "text/xml");
//		alert('DELETE: ' + server + "api/users/" + user + "/posts/" + hash + " -" + user + "/" + apikey + "\n" + url + "\n"+this.getHash(url) );
		try{
			xmlHttp.send(null);
		} catch(err) {
			alert("Couldn't connect to the server: " + server);
			return -1;
		}

		var statuscode = xmlHttp.status;
		if(statuscode != 200){
			alert("error while deleting bookmark (server) \nerror code: "+ statuscode+"\n-400 bad request \n-401 Unauthorized \n-403 Forbidden \n-500 Internal Server Error")
			return -1;
		}
		return true;
	}
	
	this.removeBookmarksFromPlaces = function(){	
		var bookmarks = this.getAllBookmarks();
		for(var i = 0; i < bookmarks.length; i++){
			this.removeFromPlaces(bookmarks[i][0] );
		}
	}
	
	
	/* Title - set by user */
	/* Tags - set by user */
	/* Description - set by user */
	/* Group - set by user */
	/* Postingdate - set by bibsonomy server */
	/* LastChange - set by bibsonomy server */
	/* Hash - set by bibsonomy server */
	/* Url - set by user */
	
	// create/update bookmark
	this.postBookmark = function(title, tags, description, group, postUrl, checkBookmark, editUrl, serverdata){
		dump("\n"+postUrl);
		// add bookmark to bibsonomy server
		var	hash;
		var	postingdate = 'not set';
		var	lastchangedate = 'not set';

		if(serverdata != null){
			hash = serverdata[0];
			postingdate = serverdata[1];
			lastchangedate = serverdata[2];
		} else { // add it on server
			hash = this.addToBibsonomyServer(title, tags, description, group, postUrl, checkBookmark, editUrl);
			if(hash === -1){
				return -1;
			}
		}
		// add to places
		bookmarkId = this.addToPlaces(title, tags, postUrl, editUrl);

		// add to own db
		this.addToOwnDatabase(bookmarkId, title, description, group, postingdate, lastchangedate, hash, postUrl, tags);
		
		return true;
	}
	
	// import/synchronize
	this.importBookmarks = function(){
		// get all bookmarks from server
		var xmlHttp = new XMLHttpRequest();
		xmlHttp.open('GET', server + "api/posts?resourcetype=bookmark&user="+user+"&start=0&end=1000000", false, user, apikey);
		try{
			xmlHttp.send(null);
		} catch (err){
			alert("No connection to the server!\n" + err);
			return -1;
		}

		var statuscode = xmlHttp.status;
		if(statuscode != 200) {
			alert("error while importing bookmarks (server) \nerror code: "+ statuscode+"\n-400 bad request \n-401 Unauthorized \n-403 Forbidden \n-500 Internal Server Error")
			return -1;
		}

		// parse bookmarks and add to local database
		var response = xmlHttp.responseXML.documentElement;
		var navItems = response.getElementsByTagName("post");
		var urlArray = new Array();
		var importedIdsArray = new Array();
				
		for (var i = 0; i < navItems.length; i++) {
			var title = navItems[i].getElementsByTagName('bookmark')[0].getAttribute('title');
			var tags = new Array ();
			for(var j = 0; navItems[i].getElementsByTagName('tag').length > j; j++){
				tags.push(navItems[i].getElementsByTagName('tag')[j].getAttribute('name'));
			}
			var description = navItems[i].getAttribute('description');
			var group = navItems[i].getElementsByTagName('group')[0].getAttribute('name');
			var url = navItems[i].getElementsByTagName('bookmark')[0].getAttribute('url');
						
			var postingdate = navItems[i].getAttribute('postingdate');
			var lastchangedate = navItems[i].getAttribute('postingdate');
			
			var hash = navItems[i].getElementsByTagName('bookmark')[0].getAttribute('interhash');
			this.postBookmark(title, tags, description, group, url, -2, url, [hash, postingdate, lastchangedate]);
			
			importedIdsArray.push(this.getBookmarkId(url));
		}
		
		// search and delete for bookmarks which got deleted on the server but are still in the local database
		var existingIdsArray = new Array();
		existingIdsArray = this.getAllBookmarks();
		
		for(var i = 0; i < existingIdsArray.length; i++){
			if(!this.find(existingIdsArray[i][0], importedIdsArray)){
				this.removeFromOwnDatabase(existingIdsArray[i][0]);
				this.removeFromPlaces(existingIdsArray[i][0]);
			}
		} 
	}
	
	// delete bookmark from server/places/own-databases
	this.deleteBookmark = function(bookmarkId, places) {
		this.removeFromBibsonomyServer(this.getUrl(bookmarkId));
		this.removeFromOwnDatabase(bookmarkId);
		if(places === false){
			return;
		}
		this.removeFromPlaces(bookmarkId);
	}

	/*
	 * helper functions
	 */

	// create md5 hash
	this.md5 = function(str) {
		//example from http://developer.mozilla.org/index.php?title=En/NsICryptoHash&highlight=nsICryptoHash
		// 'hello world' should be '5eb63bbbe01eeed093cb22bb8f5acdc3'
		var converter =
			Components.classes["@mozilla.org/intl/scriptableunicodeconverter"].
			createInstance(Components.interfaces.nsIScriptableUnicodeConverter);
		converter.charset = "UTF-8"; // we use UTF-8 here, you can choose other encodings.

		var result = {}; // result.value will contain the array length
		var data = converter.convertToByteArray(str, result); // data is an array of bytes
		var ch = Components.classes["@mozilla.org/security/hash;1"]
								.createInstance(Components.interfaces.nsICryptoHash);
		ch.init(ch.MD5);
		ch.update(data, data.length);
		var hash = ch.finish(false);

		// return the two-digit hexadecimal code for a byte
		function toHexString(charCode)
		{
			return ("0" + charCode.toString(16)).slice(-2);
		}

		// convert the binary hash data to a hex string.
		var s = [toHexString(hash.charCodeAt(i)) for (i in hash)].join("");
		return s.substring(0,32);
	}
	
	// time measuring	
	function duration2String(nMS){
		var sReturn, sComp, nDur;
	
		nDur = nMS % 1000;
		sComp = nDur.toString();
		while (sComp.length<3)
			sComp = "0" + sComp;
		sReturn = "." + sComp + " seconds";
	
		// Strip off last component
		nMS -= nDur;
		nMS /= 1000;
	
		nDur = nMS % 60;
		if (nDur)
			sReturn = nDur.toString() + sReturn;
		else
			sReturn = "0" + sReturn;
	
		// Strip off last component
		nMS -= nDur;
		nMS /= 60;
	
		nDur = nMS % 60;
		sReturn = nDur.toString() + " minutes, and " + sReturn;
	
		// Strip off last component
		nMS -= nDur;
		nMS /= 60;
	
		return nMS.toString() + " hours, " + sReturn;
	}
	
	// build a xml document which is needed to post a new bookmark
	// http://www.bibsonomy.org/help/doc/methods/CreatePost.html - XML-Schema
	/*	&      &amp;
	 *	'      &apos;
	 *	<      &lt;
	 *	>      &gt;
	 *	"      &quot;
	 */
	this.buildXmlPostBookmarkDocument = function(description, user, tags, group, url, title){
		var xmlHeader = "<?xml version='1.0'?>\n<bibsonomy>\n";
		var xmlPost = "\t<post description='" + this.replaceXmlChars(description) + "'>\n";
		var xmlUser = "\t\t<user name='" + this.replaceXmlChars(user) + "'/>\n";
		var xmlTags = '';
		for(var i=0; i < tags.length ;i++){
			xmlTags = xmlTags + "\t\t<tag name='" + this.replaceXmlChars(tags[i]) + "'/>\n";
		}
		var xmlGroups = '';
		for(var i=0; i < group.length ;i++){
			xmlGroups = xmlGroups + "\t\t<group name='" + this.replaceXmlChars(group[i]) + "'/>\n";
		}
		//var xmlGroup = "\t\t<group name='" + this.replaceXmlChars(group) + "'/>\n";
		var xmlBookmark = "\t\t<bookmark url='" + this.replaceXmlChars(url) + "' title='" + this.replaceXmlChars(title) + "'/>\n";
		var xmlFooter =  "\t</post>\n</bibsonomy>";
		var xmlDocument = xmlHeader + xmlPost + xmlUser + xmlTags + xmlGroups + xmlBookmark + xmlFooter;
		return xmlDocument;
	}
	
	this.replaceXmlChars = function(string){
		return string.replace(/&/g,'&amp;').replace(/'/g,'&apos;').replace(/</g,'&lt;').replace(/>/g,'&gt;').replace(/"/g,'&quot;');
	}

	// compare two bookmarks if they equal
	this.isSameBookmark = function(bookmarkOne, bookmarkTwo){
		var tagsArrayOne = new Array ();
		for(var j = 0; bookmarkOne.getElementsByTagName('tag').length > j; j++){
			tagsArrayOne.push(bookmarkOne.getElementsByTagName('tag')[j].getAttribute('name'));
		}
		var descriptionOne = bookmarkOne.getElementsByTagName('post')[0].getAttribute('description');
		var urlOne = bookmarkOne.getElementsByTagName('bookmark')[0].getAttribute('url');
		var titleOne = bookmarkOne.getElementsByTagName('bookmark')[0].getAttribute('title');
		var groupOne = bookmarkOne.getElementsByTagName('group')[0].getAttribute('name');

		var tagsArrayTwo = new Array ();
		for(var j = 0; bookmarkTwo.getElementsByTagName('tag').length > j; j++){
			tagsArrayTwo.push(bookmarkTwo.getElementsByTagName('tag')[j].getAttribute('name'));
		}
		var descriptionTwo = bookmarkTwo.getElementsByTagName('post')[0].getAttribute('description');
		var urlTwo = bookmarkTwo.getElementsByTagName('bookmark')[0].getAttribute('url');
		var titleTwo = bookmarkTwo.getElementsByTagName('bookmark')[0].getAttribute('title');
		var groupTwo = bookmarkTwo.getElementsByTagName('group')[0].getAttribute('name');

		if(descriptionOne != descriptionTwo) return false;
		if(urlOne != urlTwo) return false;
		if(titleOne != titleTwo) return false;
		if(groupOne != groupTwo) return false;
		if(tagsArrayOne.length != tagsArrayTwo.length) return false;
		for(var j = 0; j < tagsArrayOne.length; j++){
			if(tagsArrayOne[j] != tagsArrayTwo[j]) return false;
		}
		return true;
	}
	
	/**********************
	 * Server connections *
	 **********************/
	this.openServerConnection = function(methode, url){
		//alert("Methode: <"+methode+">\nUrl: <"+url+">\nUser: <"+user+">\nApikey: <"+apikey+">");
		//http://www.bibsonomy.org/ajax/getBookmarkRecommendedTags?url=http://www.welt.de/
		var xmlHttp = new XMLHttpRequest();
		xmlHttp.open(methode, url , false, user, apikey);
		try{
			xmlHttp.send(null);
		} catch (err){
			alert("No connection to the server!\n");
			return -1;
		}

		var statuscode = xmlHttp.status;
		if(statuscode != 200) {
			alert("No connection to the server \nerror code: "+ statuscode+"\n-400 bad request \n-401 Unauthorized \n-403 Forbidden \n-500 Internal Server Error")
			return -1;
		}
		// parse bookmarks and add to local database
		var response = xmlHttp.responseXML.documentElement;
		return response;
	}

	// check if the url is already bookmarked (on bibsonomy server) and return the bookmark details 
	this.isBookmarked = function(urlToCheck){
		// create md5 hash for url
		var hash = this.md5(urlToCheck);
		var url = server + "api/posts?resourcetype=bookmark&user="+user+"&resource="+hash;
		var response = this.openServerConnection('GET',url);
		if(response == -1){
			return -1;
		}
		var posted = response.getElementsByTagName('posts')[0].getAttribute('end');
		if(posted == 0){
			return false;
		} else {
			return response;
		}
	}
	
	this.getGroups = function () {
		//return new Array();
		var url = server + "api/users/" + user;
		var response = this.openServerConnection('GET',url);
		if(response == -1){
			return -1;
		}
		var navItems = response.getElementsByTagName("group");
		var groups = new Array();
		for (var i = 0; i < navItems.length; i++) {
			groups.push(name = navItems[i].getAttribute('name'));
		}
		return groups;
	}
	
	var recommendationDocument;
	var recommendationXmlHttp;
	this.getTagRecommendation = function(urlToCheck, document_){
		recommendationDocument = document_;
		var url = server + "ajax/getBookmarkRecommendedTags?post.resource.url=" + urlToCheck;
		recommendationXmlHttp = new XMLHttpRequest();
		recommendationXmlHttp.open('GET', url , true, user, apikey);
		recommendationXmlHttp.onreadystatechange = this.eventHandlerTagRecommendation;
		try{
			recommendationXmlHttp.send(null);
		} catch (err){
			alert("No connection to the server!\n");
			return -1;
		}

	}
	
	this.eventHandlerTagRecommendation = function(){
		var statuscode = recommendationXmlHttp.status;
		if(statuscode != 200) {
			alert("No connection to the server \nerror code: "+ statuscode+"\n-400 bad request \n-401 Unauthorized \n-403 Forbidden \n-500 Internal Server Error")
			return -1;
		}
		
		// parse the results
		var response = recommendationXmlHttp.responseXML.documentElement;
		var navItems = response.getElementsByTagName("tag");
		var tagRecommendation = new Array();
		for (var i = 0, globalcount = 0; i < navItems.length; i++, globalcount++) {
			var name = navItems[i].getAttribute('name');
			tagRecommendation.push(name);
		}

		// clear the recommendationlist
		var rowRecommendation = recommendationDocument.getElementById('rowRecommendation');
		while (rowRecommendation.firstChild) {
			rowRecommendation.removeChild(rowRecommendation.firstChild);
		}
		// add new element to the recommendationlist
		for(var i = 0; i < tagRecommendation.length; i++ ){
			var descr = recommendationDocument.createElement('description');
			descr.setAttribute('value', tagRecommendation[i]);
			descr.setAttribute('class', 'tagRecommendation');
			descr.setAttribute('onmouseup',"addRecommendationtags('" + escape(tagRecommendation[i]) + "')");
			rowRecommendation.appendChild(descr);
		}
	}


	this.getScraperInfo = function(){
		if(Preferences.getScraperDate() < (new Date).getDay()){
			//http://scraper.bibsonomy.org/service?action=info&format=json
			var xmlHttp = new XMLHttpRequest();
			xmlHttp.open('GET', server.replace('www','scraper') + "service?action=info&format=json" , false, user, apikey);
			try{
				xmlHttp.send(null);
			} catch (err){
				alert("No connection to the server!\n" + err);
				return -1;
			}
			var statuscode = xmlHttp.status;
			if(statuscode != 200) {
				alert("error while getting Groups (server) \nerror code: "+ statuscode+"\n-400 bad request \n-401 Unauthorized \n-403 Forbidden \n-500 Internal Server Error")
				return -1;
			}

			Preferences.setScraperDate((new Date).getDay());
			Preferences.setScraper(xmlHttp.responseText);
			return xmlHttp.responseText;
		} else {
			return Preferences.getScraper();
		}
	}

	this.importRelations = function(){
		var url = server + "api/users/" + user + "/concepts";
		var response = this.openServerConnection('GET', url);
		if(response == -1){
			return -1;
		}
		
		var navItems = response.getElementsByTagName("tag");
		var relations = new Array();
		for (var i = 0, globalcount = 0; i < navItems.length; i++, globalcount++) {
			var name = navItems[i].getAttribute('name');
			subtagsCount = response.getElementsByTagName('subTags')[globalcount].getAttribute('end');
			var subtagsNames = new Array();
			for(var j = 0; j < subtagsCount; j++){
				i++;
				subtagsNames.push(navItems[i].getAttribute('name'));
			}
			this.addToRelations(name, subtagsNames.join(" "));
		//	relations.push(new Array(name, subtagsNames));
		}
		//this.addToRelations(relations);
	}
	
	/********************
	 * Database queries *
	 ********************/
	this.getHash = function(url){
		try {
			var statement = this.mDBConn.createStatement("SELECT hash FROM bibBookmarks where url=?1");
			statement.bindStringParameter(0, url);
			var searchResults = new Array();
			while (statement.executeStep()) {
				searchResults.push(statement.getString(0));
			}
		} catch (err) {
			alert("error while getting hash from own database!\n" + err);
		} finally  {
			statement.reset();
		}
		return searchResults;		
	}
	
	// returns a complete list of all bookmarks with id, title, description, url and tags 
	this.getAllBookmarks = function (bookmarkstart,bookmarklimit,bookmarkIds) {
		var orstr = '';
		if(bookmarkIds !== undefined){
			orstr += "and (";
			for(var i = 0 ; i < bookmarkIds.length; i++){
				orstr += bookmarkIds[i] + "=a.id";
				if(i < bookmarkIds.length-1){
					orstr += " or ";
				}
			}
			orstr += ") ";
		}
		var limit = '';
		if((bookmarkstart !== undefined && bookmarkstart!=='') && (bookmarklimit !== undefined && bookmarklimit!=='')){
			limit = "limit " + bookmarkstart + "," + bookmarklimit ;
		}
		try {
			var statement = this.mDBConn.createStatement("SELECT a.id,a.title,a.description,a.url, group_concat(b.tag,' ') FROM bibBookmarks a, bibTags b where a.id=b.id " + orstr + "group by 1 ORDER BY a.title " + limit);
			var searchResults = new Array();
			while (statement.executeStep()) {
				searchResults.push(new Array(statement.getString(0),statement.getString(1),statement.getString(2),statement.getString(3),statement.getString(4)));
			}
		} catch (err) {
			alert("error while getting all bookmarks from own database!\n" + err);
		} finally  {
			statement.reset();
		}
		return searchResults;		
	}
	
	this.readOwnBookmarks = function () {
		try {
			var statement = this.mDBConn.createStatement("SELECT * FROM bibBookmarks");
			var searchResults = new Array();
			while (statement.executeStep()) {
				var searchResultRow = new Array();
				for(var i = 0; i < statement.columnCount; i++){
					searchResultRow.push(statement.getString(i));
				}
				searchResults.push(searchResultRow);
			}
			
			var colNames = new Array();
			for(var j = 0; j < statement.columnCount; j++){
				colNames.push(statement.getColumnName(j))
			}
			searchResults.push(colNames);

		} catch (err) {
			alert("error while getting all bookmarks from own database!\n" + err);
		} finally  {
			statement.reset();
		}
		return searchResults;		
	}
	
	this.readOwnTags = function () {
		try {
			var statement = this.mDBConn.createStatement("SELECT * FROM bibTags");
			var searchResults = new Array();
			while (statement.executeStep()) {
				var searchResultRow = new Array();
				for(var i = 0; i < statement.columnCount; i++){
					searchResultRow.push(statement.getString(i));
				}
				searchResults.push(searchResultRow);
			}
			
			var colNames = new Array();
			for(var j = 0; j < statement.columnCount; j++){
				colNames.push(statement.getColumnName(j))
			}
			searchResults.push(colNames);

		} catch (err) {
			alert("error while getting all bookmarks from own tag database!\n" + err);
		} finally  {
			statement.reset();
		}
		return searchResults;		
	}
	
	this.readPlaces = function () {
         var options = historyService.getNewQueryOptions();
         var query = historyService.getNewQuery();
         query.setFolders([bookmarks.bookmarksMenuFolder], 1);
         query.setFolders([bookmarks.toolbarFolder], 1);
         query.setFolders([bookmarks.unfiledBookmarksFolder], 1);
         //[bookmarks.toolbarFolder,bookmarks.unfiledBookmarksFolder, bookmarks.bookmarksMenuFolder]
         var result = historyService.executeQuery(query, options);
         var rootNode = result.root;
         rootNode.containerOpen = true;

         // iterate over the immediate children of this folder and dump to console
         var searchResults = new Array();
         searchResults.push(new Array('id', 'title', 'uri', 'node_type'));
         var string = '';
         for (var i = 0; i < rootNode.childCount; i ++) {
        	 var node = rootNode.getChild(i);
        	 // -1 if it is no bookmark, folder, or separator
        	 //if(node.itemId != -1)
             searchResults.push(new Array(node.itemId, node.title , node.uri, node.type));        	 
         }
         // close a container after using it!
         rootNode.containerOpen = false;
		
         return searchResults;		
	}
	
	// returns the title for a bookmarkid
	this.getTitle = function(bookmarkId){
		var title = null;
		try{
			var statement = this.mDBConn.createStatement("SELECT title FROM " + dbName + " WHERE id=?1");
			statement.bindStringParameter(0, bookmarkId);
			while (statement.executeStep()) {
				title = statement.getString(0);
			}
		} catch (err) {
			alert("error to get title for ids (owntagdb)"+err);
		} finally {
			statement.reset();
		}
		return title;
	}

	// returns all tags for a bookmarkid
	this.getTags = function(bookmarkId){
		try{
			var myTags = new Array();
			var statement = this.mDBConn.createStatement("SELECT tag FROM " + dbNameTags + " WHERE id=?1");
			statement.bindStringParameter(0, bookmarkId);
			while (statement.executeStep()) {
				tag = statement.getString(0);
				myTags.push(tag);
			}
		} catch (err) {
			alert("error to get tags for ids (owntagdb)"+err);
		} finally {
			statement.reset();
		}	
		return myTags;
	}
	
	// returns all tags
	this.getTaglist = function(){
		try{
			var statement = this.mDBConn.createStatement("SELECT * FROM (SELECT tag, count(tag) as c  FROM " + dbNameTags + " GROUP BY 1 ORDER BY c) ORDER BY 1");
			var searchResults = new Array();
			while (statement.executeStep()) {
				searchResults.push(new Array(statement.getString(0), statement.getString(1)));
			}
		} catch (err) {
			alert("error while searching in own tag database!\n" + err);
		} finally {
			statement.reset();
		}
		return searchResults;
	}
	
	// returns all tags and the tags related with it
	this.getTaglistLike = function(str){
		try{
			var rstr=str.split(" ");

			var orstr="";
			var istr=new Array();
			for (var i=0,c=0;i<rstr.length;i++){
				if (rstr[i]!=""){
					istr.push(rstr[i]);
					if (orstr!="")
						orstr+=" or ";
					orstr+=" FIELD like ?" + (c+1) +" ";
					c++;
				}
			}
			orstr = " ("  + orstr + ") "; 
			orstr = orstr.replace(/FIELD/g,"a.tag");
			sta = "SELECT * from (select c.tag,count(c.tag) as c from bibtags a, bibtags c where  ( a.tag like ?1)  and a.id = c.id GROUP BY 1 ORDER BY c desc ) ORDER BY 1";
//			sta = "SELECT DISTINCT tag FROM " + dbNameTags + " WHERE " + orstr + " ORDER BY 1";
			var statement = this.mDBConn.createStatement(sta);			
			for(var i = 0; i < istr.length; i++){
				statement.bindStringParameter(i,"%"+istr[i]+"%");
			}
			var searchResults = new Array();
			while (statement.executeStep()) {
				searchResults.push(new Array(statement.getString(0), statement.getString(1)));
			}
		} catch (err) {
			alert("error while searching in own tag database!\n" + err);
		} finally {
			statement.reset();
		}
		return searchResults;
	}

	// returns a 'count' long list with tag and the count it occurs  
	this.getCloudTags = function(count){
		try{
			var statement = this.mDBConn.createStatement("SELECT * from (SELECT tag,count(tag) as c FROM " + dbNameTags + " GROUP BY 1 ORDER BY c desc LIMIT ?1) ORDER BY 1");
			statement.bindInt32Parameter(0,count);
			var searchResults = new Array();
			while (statement.executeStep()) {
				searchResults.push(new Array(statement.getString(0),statement.getString(1)));
			}
		} catch (err) {
			alert("error while searching in own tag database!\n" + err);
		} finally {
			statement.reset();
		}
		return searchResults;
	}

	// returns a 'count' long list with tag, the count it occurs and the tags related with it  
	this.getCloudTagsLike = function(count, str){
		try{ 
			var rstr=str.split(" ");

			var orstr="";
			var istr=new Array();
			for (var i=0,c=0;i<rstr.length;i++){
				if (rstr[i]!=""){
					istr.push(rstr[i]);
					if (orstr!="")
						orstr+=" or ";
					orstr+=" FIELD like ?" + (c+2) +" ";
					c++;
				}
			}
			orstr = " ("  + orstr + ") "; 
			orstr = orstr.replace(/FIELD/g,"a.tag");
			sta = "SELECT * from (select c.tag,count(c.tag) as c from bibtags a, bibtags c where " + orstr + " and a.id = c.id GROUP BY 1 ORDER BY c desc LIMIT ?1) ORDER BY 1";
//	old		sta = "SELECT * from (select c.tag,count(c.tag) as c from bibtags a, bibtags c where a.tag like ?2 and a.id = c.id GROUP BY 1 ORDER BY c desc LIMIT ?1) ORDER BY 1";
			var statement = this.mDBConn.createStatement(sta);			
			statement.bindInt32Parameter(0,count);
			for(var i = 1; i < istr.length+1; i++){
				statement.bindStringParameter(i,"%"+istr[i-1]+"%");
			}

			var searchResults = new Array();
			while (statement.executeStep()) {
				searchResults.push(new Array(statement.getString(0),statement.getString(1)));
			}
		} catch (err) {
			alert("error while searching in own tag database!\n" + err);
		} finally {
			statement.reset();
		}
		return searchResults;
	}

	this.getTagSuggestions = function(str, count){
		try{ 
			//sta = "SELECT * from (select c.tag, count(c.tag) as c from bibtags a, bibtags c where ( a.tag like ?1 ) and a.id = c.id GROUP BY 1 ORDER BY c desc LIMIT ?2) ORDER BY 1";
			sta = "SELECT tag, count(tag) as c FROM bibtags a WHERE a.tag LIKE ?1 GROUP BY 1 ORDER BY c desc LIMIT ?2"
			var statement = this.mDBConn.createStatement(sta);			
			statement.bindStringParameter(0,str+"%");
			statement.bindInt32Parameter(1,count);
			var searchResults = new Array();
			while (statement.executeStep()) {
				searchResults.push(new Array(statement.getString(0)));
			}
		} catch (err) {
			alert("error while searching in own tag database!\n" + err);
		} finally {
			statement.reset();
		}
		return searchResults;
	}
	//"SELECT * FROM (SELECT c.tag count(c.tag) AS c FROM bibtags LIMIT ?1)"
	
	// returns the id, title, description, url and tag of all bookmarks which taged to the given tag
	this.getBookmarksForTag = function(tag,bookmarkstart,bookmarklimit){
		var tags = tag.split(" ");
		var orstr = '';
		for(var i = 0; i < tags.length; i++){
			orstr+= " a.tag like ?" + (i+1) + " ";
			if(i <tags.length-1){
				orstr += " or ";
			}
		}
		orstr = " (" + orstr + ") ";
		var limit = '';
		if((bookmarkstart !== undefined && bookmarkstart!=='') && (bookmarklimit !== undefined && bookmarklimit!=='')){
			limit = "limit " + bookmarkstart + "," + bookmarklimit ;
		}
		
		// id, title, description, url, tag
		var searchResult = new Array();
		try{
			var stat = "select a.id,a.title,a.description,a.url,group_concat(b.tag,',') from (SELECT DISTINCT b.id as 'id',b.title  as 'title',b.description as 'description' ,b.url as 'url',count(DISTINCT a.tag) as 'tagcount' FROM bibTags a, bibBookmarks b WHERE  " + orstr + " and a.id=b.id group by 1 having tagcount>=1 ) a,bibTags b where a.id=b.id group by 1 order by 2 " + limit;
			var statement = this.mDBConn.createStatement(stat);
			for(var j = 0; j < tags.length; j++){
				statement.bindStringParameter(j, tags[j]);
			}	
			while (statement.executeStep()) {
				searchResult.push(new Array(statement.getString(0),statement.getString(1),statement.getString(2),statement.getString(3),statement.getString(4)));
			}
		} catch (err) {
			alert("error while searching in own tag database!\n" + err);
		} finally {
			statement.reset();
		}
		return searchResult;
	}
	
	// returns the description for a bookmarkid
	this.getDescription = function(bookmarkId){
		var result = null;
		try {
			var statement = this.mDBConn.createStatement("SELECT description FROM " + dbName + " WHERE id = ?1");
			statement.bindInt32Parameter(0,bookmarkId);
			while (statement.executeStep()) {
				 result = statement.getString(0);
			}
		} catch (err){
			alert("error while searching in own database!\n" + err);
		} finally {
			statement.reset();
		}
		return result;
	}

	// returns the url for a bookmarkid
	this.getUrl = function(bookmarkId){
		var result = null;
		try {
			var statement = this.mDBConn.createStatement("SELECT url FROM " + dbName + " WHERE id = ?1");
			statement.bindInt32Parameter(0,bookmarkId);
			while (statement.executeStep()) {
				return statement.getString(0);
			}
		} catch (err){
			alert("error while searching in own database!\n" + err);
		} finally {
			statement.reset();
		}
		return result;
	}
	
	// returns the bookmarkid for a url
	this.getBookmarkId = function(url){
		var result = null;
		try {
			var statement = this.mDBConn.createStatement("SELECT id FROM " + dbName + " WHERE url = ?1");
			statement.bindStringParameter(0 , url);
			while (statement.executeStep()) {
				return statement.getInt32(0);
			}
		} catch (err){
			alert("error while searching in own database!\n" + err);
		} finally {
			statement.reset();
		}
		return result;
	}

	this.getRelations = function(){
		if(Preferences.getRelationDate() < (new Date).getDay()){
			this.importRelations();
			Preferences.setRelationDate((new Date).getDay());
		}
		var relations = new Array();
		try {
			var statement = this.mDBConn.createStatement("SELECT supertag, subtags FROM " + dbNameRelations);
			while (statement.executeStep()) {
				relations.push(statement.getString(0) + " " + statement.getString(1));
			}
		} catch (err){
			alert("error while getting relations!\n" + err);
		} finally {
			statement.reset();
		}
		return relations;
	}

	this.addToRelations = function(supertag, subtags){
		try{
			var statement = this.mDBConn.createStatement("INSERT OR REPLACE INTO " + dbNameRelations + " VALUES(?1,?2)");
			statement.bindStringParameter(0, supertag);
			statement.bindStringParameter(1, subtags);
			statement.execute();
		} catch (err) {
			alert("error while adding relation to database \n" + err);
		} finally {
			statement.reset();
		}
	}

	
	// returns the folder where the bibsonomy-bookmarks are stored in the placessystem
	this.getDefaultFolder = function (){
		/* 
		 * atm the default folder is by default an invisible folder from firefox 'unfiledBookmarksFolder'.
		 * If you want to to show them in the menu- or toolbarfolder you have to add another folder to the
		 * bookmarks. The folder to save them can be returned here. 
		 */
/*		if(Preferences.getDefaultFolderId() == ""){
			if(Preferences.getDefaultFolder == -1){
				Preferences.setDefaultFolderId(bookmarks.unfiledBookmarksFolder);
				var bibfolder = bookmarks.unfiledBookmarksFolder;
			} else {
		   		var bibfolder = bookmarks.createFolder(bookmarks.bookmarksMenuFolder, Preferences.getDefaultFolder(), -1);
		   		Preferences.setDefaultFolderId(bibfolder);
			}
	   		return bibfolder;
		} else {
			return Preferences.getDefaultFolderId();
		}*/ 
		return bookmarks.unfiledBookmarksFolder;
	}

	// searchs for description, title, url and/or tags
	this.search = function(str, isUrl, isTitle, isDescription, isTags, bookmarkstart, bookmarklimit) {
		var limit = '';
		if((bookmarkstart !== undefined && bookmarkstart!=='') && (bookmarklimit !== undefined && bookmarklimit!=='')){
			limit = "limit " + bookmarkstart + "," + bookmarklimit ;
		}
		
		var searchResults = new Array();
		if(isDescription || isTitle || isUrl || isTags){
			try{
				
				var rstr=str.split(" ");
				
				var orstr="";
				var istr=new Array();
				for (var i=0,c=0;i<rstr.length;i++){
					if (rstr[i]!=""){
						istr.push(rstr[i]);
						if (orstr!="")
							orstr+=" or ";
						orstr+=" FIELD like ?" + (c+1) +" ";
						c++;
					}
				}
				orstr=" ("  + orstr + ") "; 
				
				var andstr="";
				for (var i=0;i<istr.length;i++){
					if (andstr!="")
						andstr+=" and ";
					andstr+=" FIELD like ?" + (i+1) +" ";
				}
				andstr=" ("  + andstr + ") "; 
				
				var stat0="";
				
				if(isDescription || isTitle || isUrl){
					stat0="SELECT id as 'id',title as 'title',description as 'description',url as 'url','' as 'tagcount' FROM " + dbName + " WHERE ";
	
					if (isDescription)
						stat0+=" " + andstr.replace(/FIELD/g,"description");
					if (isTitle){
						if (isDescription)
							stat0+=" or ";
						stat0+=" " + andstr.replace(/FIELD/g,"title");
					}
					if (isUrl){
						if (isDescription || isTitle)
							stat0+=" or ";
						stat0+=" " + andstr.replace(/FIELD/g,"url");
					}
				}
				var stat1="";
				
				if (isTags){
					stat1="SELECT DISTINCT b.id as 'id',b.title  as 'title',b.description as 'description' ,b.url as 'url',count(DISTINCT a.tag) as 'tagcount' FROM " + dbNameTags + " a, " + dbName  + " b WHERE " + orstr.replace(/FIELD/g,"a.tag") + " and a.id=b.id group by 1 having tagcount>=" + istr.length + " " ;
				}

				var stat = stat0;
				if (stat==""){
					stat = stat1;
				}else if (stat1!=""){
					stat += " union " + stat1;
				}
				
				stat = "select a.id,a.title,a.description,a.url,group_concat(b.tag,',') from (" + stat + ") a,bibTags b where a.id=b.id group by 1 order by 2 " + limit;
				var statement = this.mDBConn.createStatement(stat );
				for (var i=0;i<istr.length;i++){
					statement.bindStringParameter(i, "%"+istr[i]+"%");
				}
				
				while (statement.executeStep()) {
					searchResults.push(
						new Array(statement.getString(0),statement.getString(1),statement.getString(2),statement.getString(3),statement.getString(4)));
				}
				
				
			} catch (err) {
				alert("error while searching for a tag (owntagdb)\n"+err + "\nStatement:" + stat);
			} finally {
				statement.reset();
			}	
		}
		return searchResults;
	}
	
	// find element in arraylist
	this.find = function(searchStr, array) {
		for(var i = 0; i < array.length; i++){
			if(array[i] == searchStr){
				return true;
			}
		}
		return false;
	}
}

